
<style type="text/css">
	.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
    color: #fff;
    background-color: #f0ad4e;
}
a{
	color: #000000;
}
</style>
<div class="col-md-2 col-sm-3">


	<div class="panel panel-default">
		<div class="panel-body">

		<ul class="nav nav-pills">


<li style="width: 100%;" <?php if($tab === 'productlist'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/productlist"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_productliststock?></font> </a></li>

<li style="width: 100%;" <?php if($tab == 'stock'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/stock">
<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_producthavestock?></font> </a></li>

<li style="width: 100%;" <?php if($tab == 'stockzero'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/stockzero">
<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_productoutstock?></font> </a></li>


<li style="width: 100%;" <?php if($tab == 'dateend'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/dateend">
<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_productenddate?></font></a></li>


<!--

<li style="width: 100%;" <?php if($tab === 'productother'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/productother"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>
สินค้าออฟชั่นเสริม </a></li> -->




<li style="width: 100%;" <?php if($tab === 'productcategory'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/productcategory"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_productcategory?></font> </a></li>

 <li style="width: 100%;" <?php if($tab === 'productunit'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/productunit"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_unit?></font> </a></li>

<li style="width: 100%;" <?php if($tab === 'supplier'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/supplier"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_supplierstock?></font> </a></li>

<li style="width: 100%;" <?php if($tab === 'zone'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/zone"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_productzone?></font> </a></li>

</ul>


		</div>
	</div>


	 <div class="panel panel-default">
		<div class="panel-body">


<ul class="nav nav-pills">

	<li style="width: 100%;" <?php if($tab === 'importproduct'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/importproduct"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
	<font style="font-family:Phetsarath OT"><?=$lang_productintostock?></font> </a></li>


	<li style="width: 100%;" <?php if($tab === 'exportproduct'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/exportproduct"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
	<font style="font-family:Phetsarath OT"><?=$lang_productgooutstock?></font> </a></li>



<!-- <li style="width: 100%;" <?php if($tab == 'showproduct'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/showproduct">
<span class="glyphicon glyphicon-link" aria-hidden="true"></span>
<?=$lang_productpagepic?> </a></li> -->



</ul>


</div>
	</div>



<div class="panel panel-default">
	 <div class="panel-body">


	<ul class="nav nav-pills">


		<li style="width: 100%;" <?php if($tab === 'reportinstock'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/reportinstock"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span>
		<font style="font-family:Phetsarath OT">ລາຍງານສິນຄ້ານຳເຂົ້າ</font>
		 </a></li>

		 <li style="width: 100%;" <?php if($tab === 'reportoutstock'){ echo 'class="active"';} ?> ><a href="<?php echo $base_url; ?>/warehouse/reportoutstock"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span>
 		<font style="font-family:Phetsarath OT">ລາຍງານສິນຄ້າອອກ</font>
 		 </a></li>
	

	</ul>


	</div>
	</div>







	</div>
