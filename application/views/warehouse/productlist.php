<font style="font-family:Phetsarath OT">
<div class="col-md-10 col-sm-9" ng-app="firstapp" ng-controller="Index">

<div class="panel panel-default">
	<div class="panel-body">



 <div style="float: right;">
	<button class="btn btn-info" ng-click="Modalexcel()">
	<font style="font-family:Phetsarath OT"><?=$lang_importproductexcel?></font></button>
</div>

<div class="form-group" style="float: left;">
<button class="btn btn-primary" ng-click="Modaladd()"><font style="font-family:Phetsarath OT">
	<?=$lang_addproduct?></font></button>
</div>


<form class="form-inline">

<div class="form-group">
<input type="text" ng-model="searchtext" class="form-control" placeholder="<?=$lang_search?>" style="width: 350px;" ng-change="getlist(searchtext,'1')">
</div>

<!-- <div class="form-group">
<button type="submit" ng-click="getlist('','1')" class="btn btn-default" placeholder="" title="<?=$lang_refresh?>"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></button>
</div> -->

</form>


<br />


 <div style="float: right;">
	<input type="checkbox" ng-model="showdeletcbut"><font style="font-family:Phetsarath OT">
	 <?=$lang_showdel?></font>
</div>
<table id="headerTable" class="table table-hover table-bordered" style="font-size: 14px;">
	<thead>
		<tr style="background-color: #eee;">
			<th style="width: 50px;"><font style="font-family:Phetsarath OT"><?=$lang_rank?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_barcode?></font><br />Print Barcode</th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_picproduct?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_productname?></font></th>
			<!-- <th style="text-align: center;">หมดอายุ</th> -->
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_detail?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_num?>(<?=$lang_edit?>)</font></th>

			 <th style="text-align: center;">
			 	<font style="font-family:Phetsarath OT"><?=$lang_productrelat?></font></th>


			<!-- <th style="text-align: center;">สินค้าเสริม</th> -->

			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_category?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
			Vendor/Supplier</font></th>
			 <th style="text-align: center;"><font style="font-family:Phetsarath OT">
			 	<?=$lang_costperunit?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_priceperunit?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_wholepriceperunit?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_score?></font></th>
			<th style="text-align: center;"><font style="font-family:Phetsarath OT">
				<?=$lang_wherestore?></font></th>
			<th style=""><font style="font-family:Phetsarath OT">
				<?=$lang_manage?></font></th>
		</tr>
	</thead>
	<tbody>




		<tr ng-repeat="x in list">
		<td ng-if="selectpage=='1'" class="text-center">{{($index+1)}}</td>
			<td ng-if="selectpage!='1'" class="text-center">{{($index+1)+(perpage*(selectpage-1))}}</td>



<td  align="center">
<font style="font-family:Phetsarath OT">{{x.product_code}}</font>
<br/>
<a href="<?php echo $base_url; ?>/warehouse/barcodemini?product_code={{x.product_code}}&product_name={{x.product_name}}&product_price={{x.product_price | number:2}}" class="btn btn-xs btn-default" target="_blank"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span><font style="font-family:Phetsarath OT"> <?=$lang_mini?></font></a>

<a href="<?php echo $base_url; ?>/warehouse/barcode?product_code={{x.product_code}}&product_name={{x.product_name}}&product_price={{x.product_price | number:2}}" class="btn btn-xs btn-default" target="_blank"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> <font style="font-family:Phetsarath OT"><?=$lang_big?></font></a>


<a href="<?php echo $base_url; ?>/warehouse/barcode_ds?product_code={{x.product_code}}&product_name={{x.product_name}}&product_price={{x.product_price | number:2}}&w_price={{x.product_wholesale_price | number:2}}" class="btn btn-xs btn-default" target="_blank"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> <font style="font-family:Phetsarath OT"><?=$lang_design?></font></a>


<a href="<?php echo $base_url; ?>/warehouse/barcode_ds_full?product_code={{x.product_code}}&product_name={{x.product_name}}&product_price={{x.product_price | number:2}}&w_price={{x.product_wholesale_price | number:2}}" class="btn btn-xs btn-default" target="_blank"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> <?=$lang_design?>2</a>


</td>



<td align="center">
<img ng-if="x.product_image!=''" ng-src="<?php echo $base_url;?>/{{x.product_image}}" width="70px" height="70px;">

			</td>

			<td><font style="font-family:Phetsarath OT">{{x.product_name}}</font>

			</td>


			<!-- <td>{{x.product_date_end}}</td> -->


			<td><font style="font-family:Phetsarath OT">{{x.product_des}}</font>

			</td>



			<td>
<button class="btn btn-primary" ng-click="Updatematmodal(x)" style="width: 120px;">
	<font style="font-family:Phetsarath OT">({{x.product_stock_num | number}}){{x.product_unit_name}}</font>
</button>
<br />
<center>
<font style="font-family:Phetsarath OT"><?=$lang_alertwhen?></font>
<br />
<b style="color:red"><font style="font-family:Phetsarath OT">{{x.product_num_min | number}}</font></b>
</center>
</td>


 <td>
<button class="btn btn-default" ng-click="Updatepotmodal(x)" style="width: 50px;"><font style="font-family:Phetsarath OT">
+({{x.product_num_other | number}})</font>
</button>
</td>



<!-- <td>
<button class="btn btn-primary" ng-click="Updatepotmodal(x)" style="width: 120px;">
+สินค้าเสริม({{x.product_num_other | number}})
</button>
</td> -->


			<td><font style="font-family:Phetsarath OT">{{x.product_category_name}}</font></td>




<td>
<font style="font-family:Phetsarath OT">{{x.supplier_name}}</font>
</td>

			 <td align="right"><font style="font-family:Phetsarath OT">{{x.product_pricebase | number:2}}</font></td>
			<td align="right"><font style="font-family:Phetsarath OT">{{x.product_price | number:2}}</font></td>
			<td align="right"><font style="font-family:Phetsarath OT">{{x.product_wholesale_price | number:2}}</font></td>
			<td align="right"><font style="font-family:Phetsarath OT">{{x.product_score | number}}</font></td>

			<td align="right"><font style="font-family:Phetsarath OT">{{x.zone_name}}</font></td>

			<td>

				<button class="btn btn-xs btn-warning" ng-click="Editinputproduct(x)"><font style="font-family:Phetsarath OT"><?=$lang_edit?></font></button>
				<button ng-show="showdeletcbut" class="btn btn-xs btn-danger" ng-click="Deleteproduct(x.product_id)">
					<font style="font-family:Phetsarath OT"><?=$lang_delete?></font></button>
			</td>

</tr>


</tbody>
</table>







<form class="form-inline">
<div class="form-group">
<font style="font-family:Phetsarath OT"><font style="font-family:Phetsarath OT"><?=$lang_show?></font></font>
<select class="form-control" name="" id="" ng-model="perpage" ng-change="getlist(searchtext,'1',perpage)">
	<option value="10">10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option value="50">50</option>
	<option value="100">100</option>
	<option value="200">200</option>
	<option value="300">300</option>
	<option value="1000">1000</option>
	<option value="3000">3000</option>
	<option value="5000">5000</option>
	<option value="10000">10000</option>
	<option value="100000">100000</option>
	<option value="1000000">1000000</option>
</select>

<font style="font-family:Phetsarath OT"><font style="font-family:Phetsarath OT"><?=$lang_page?></font></font>
<select name="" id="" class="form-control" ng-model="selectthispage"  ng-change="getlist(searchtext,selectthispage,perpage)">
	<option  ng-repeat="i in pagealladd" value="{{i.a}}">{{i.a}}</option>
</select>
</div>


</form>




<hr />
<button id="btnExport" class="btn btn-default" onclick="fnExcelReport();"> <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_downloadexcel?></font> </button>



<div class="modal fade" id="updatematmodal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><font style="font-family:Phetsarath OT"><?=$lang_edit?> {{matdata.product_name}}</font></h4>
			</div>
			<div class="modal-body">

				<center>
<h2><font style="font-family:Phetsarath OT"><?=$lang_num?></font></h2>
<input type="text" ng-model="matdata.product_stock_num" class="form-control" style="font-size: 25px;text-align: center;">
<br />
<button class="btn btn-success" ng-click="Updatematok()"><font style="font-family:Phetsarath OT">
	<?=$lang_save?></font></button>

</center>

			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>









<div class="modal fade" id="updatepotmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?=$lang_productrelat?> {{potdata.product_name}} 1 {{potdata.product_unit_name}}</h4>
			</div>
			<div class="modal-body">
<font style="font-family:Phetsarath OT"><?=$lang_relaydetail?></font>
<input type="text" placeholder="ຄົ້ນຫາສິນຄ້າ" class="form-control" ng-model="search_pot" ng-change="Searchpot(search_pot)">

<table class="table">
	<tr ng-repeat="x in getpotlist">
		<td><button ng-click="Addpot(x)" class="btn btn-xs btn-success" ><font style="font-family:Phetsarath OT"><?=$lang_add?></font></button></td>

			<td><font style="font-family:Phetsarath OT">{{x.product_name}}</font></td>
				<td>
<input type="number" placeholder="ຈຳນວນ" ng-model="x.product_num_relation" class="form-control">
				</td>
				<td><font style="font-family:Phetsarath OT">{{x.product_unit_name}}</font></td>
	</tr>
</table>
<hr />
<?=$lang_productrelat?> {{potdata.product_name}} 1 {{potdata.product_unit_name}}

<table class="table">
	<tr ng-repeat="x in getproductpotlist">
			<td><font style="font-family:Phetsarath OT">{{x.product_name_relation}}</font></td>
				<td><font style="font-family:Phetsarath OT">{{x.product_num_relation}}</font></td>
				<td><font style="font-family:Phetsarath OT">{{x.product_unit_relation}}</font></td>
				<td><button ng-click="Delpot(x)" class="btn btn-xs btn-danger" >
					<font style="font-family:Phetsarath OT"><?=$lang_delete?></font></button></td>
	</tr>
</table>

</center>

			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>



<div class="modal fade" id="Openadd">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><font style="font-family:Phetsarath OT">
					<?=$lang_addproduct?></font></h4>
			</div>
			<div class="modal-body">
				<form id="uploadImg"  enctype="multipart/form-data" method="POST">

<font style="font-family:Phetsarath OT"><?=$lang_barcode?></font>
<input type="text" name="product_code"  placeholder="<?=$lang_barcode?>" class="form-control">
<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_picproduct?></font>
<input type="file" name="product_image" accept="image/*" class="form-control" value="">
<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_productname?></font>
<input type="text" name="product_name"  placeholder="<?=$lang_productname?>" class="form-control" required="required">
<p></p>


<input type="hidden" id="product_date_end" name="product_date_end"  placeholder="ວັນໝົດອາຍຸ" class="form-control">

<font style="font-family:Phetsarath OT"><?=$lang_productunit?></font>
<select class="form-control" name="product_unit_id" >
<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="y in unitlist" value="{{y.product_unit_id}}">
						<font style="font-family:Phetsarath OT">{{y.product_unit_name}}</font>
					</option>
				</select>
<p></p>

<font style="font-family:Phetsarath OT"><?=$lang_detail?></font>
<textarea type="text" class="form-control" name="product_des">
</textarea>
<p></p>


<font style="font-family:Phetsarath OT"><?=$lang_category?></font>
<select class="form-control" name="product_category_id" >
<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_selectcategory?></font></option>
					<option ng-repeat="y in categorylist" value="{{y.product_category_id}}">
						<font style="font-family:Phetsarath OT">{{y.product_category_name}}</font>
					</option>
				</select>
<p></p>

<font style="font-family:Phetsarath OT">Supplier</font>
<select class="form-control" name="supplier_id" >
				<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="x in supplierlist" value="{{x.supplier_id}}">
						<font style="font-family:Phetsarath OT">{{x.supplier_name}}</font>
					</option>
				</select>



<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_cost?></font>
	<input type="text" name="product_pricebase"  placeholder="<?=$lang_cost?>" class="form-control text-right">



	<p></p>


	<font style="font-family:Phetsarath OT"><?=$lang_priceperunit?></font>
	<input type="text" name="product_price"  placeholder="<?=$lang_priceperunit?>" class="form-control text-right">
<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_wholepriceperunit?></font>
	<input type="text" name="product_wholesale_price"  placeholder="<?=$lang_wholepriceperunit?>" class="form-control text-right">

<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_score?></font>
	<input type="text" name="product_score"  placeholder="<?=$lang_score?>" class="form-control text-right">

	<p></p>

<font style="font-family:Phetsarath OT">Zone</font>
<select class="form-control" name="zone_id" >
				<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="x in zonelist" value="{{x.zone_id}}">
						<font style="font-family:Phetsarath OT">{{x.zone_name}}</font>
					</option>
				</select>

	<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_alertwhennum?></font>
		<input type="text" name="product_num_min"  placeholder="0" class="form-control text-right">

		<p></p>

<button class="btn btn-success" type="submit"><font style="font-family:Phetsarath OT">
	<?=$lang_save?></font></button>
</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><font style="font-family:Phetsarath OT">Close</font></button>

			</div>
		</div>
	</div>
</div>





<div class="modal fade" id="Openedit">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><font style="font-family:Phetsarath OT"><?=$lang_edit?></font></h4>
			</div>
			<div class="modal-body">
				<form id="Updatedata"  enctype="multipart/form-data" method="POST">

<input type="hidden" name="product_id" id="product_id">
<font style="font-family:Phetsarath OT"><?=$lang_barcode?></font>
<input type="text" name="product_code" id="product_code" placeholder="<?=$lang_barcode?>" class="form-control">
<p></p>
<input type="hidden" name="product_image2" id="product_image2">
<center>
<img ng-if="product_image!=''" ng-src="<?php echo $base_url;?>/{{product_image}}" width="70px" height="70px;">
</center>
<font style="font-family:Phetsarath OT"><?=$lang_picproduct?></font>
<input type="file" name="product_image" accept="image/*" class="form-control" value="">
<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_productname?></font>
<input type="text" name="product_name" id="product_name" placeholder="<?=$lang_productname?>" class="form-control" required="required">
<p></p>


<input type="hidden" name="product_date_end" id="product_date_end2" placeholder="ວັນໝົດອາຍຸ" class="form-control">


<font style="font-family:Phetsarath OT"><?=$lang_productunit?></font>
<select class="form-control" name="product_unit_id" id="product_unit_id" >
<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="y in unitlist" value="{{y.product_unit_id}}">
						<font style="font-family:Phetsarath OT">{{y.product_unit_name}}</font>
					</option>
				</select>
<p></p>



<font style="font-family:Phetsarath OT"><?=$lang_detail?></font>
<textarea type="text" class="form-control" name="product_des" id="product_des">
</textarea>
<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_category?></font>
<select class="form-control" name="product_category_id" id="product_category_id">
<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_selectcategory?></font></option>
					<option ng-repeat="y in categorylist" value="{{y.product_category_id}}">
						<font style="font-family:Phetsarath OT">{{y.product_category_name}}</font>
					</option>
				</select>
<p></p>

<font style="font-family:Phetsarath OT">Vendor/Supplier</font>
<select class="form-control" name="supplier_id" id="supplier_id">
	<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="x in supplierlist" value="{{x.supplier_id}}">
						<font style="font-family:Phetsarath OT">{{x.supplier_name}}</font>
					</option>
				</select>



<p></p>
<font style="font-family:Phetsarath OT"><?=$lang_cost?></font>
	<input type="text" name="product_pricebase" id="product_pricebase" placeholder="<?=$lang_cost?>" class="form-control text-right">


	<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_saleprice?></font>
	<input type="text" name="product_price" id="product_price" placeholder="<?=$lang_saleprice?>" class="form-control text-right">


	<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_wholepriceperunit?></font>
	<input type="text" name="product_wholesale_price" id="product_wholesale_price" placeholder="<?=$lang_wholepriceperunit?>" class="form-control text-right">

<p></p>
	<font style="font-family:Phetsarath OT"><?=$lang_score?></font>
	<input type="text" name="product_score" id="product_score" placeholder="<?=$lang_score?>" class="form-control text-right">

	<p></p>
	<font style="font-family:Phetsarath OT">Zone</font>
	<select class="form-control" name="zone_id" id="zone_id">
				<option value="0"><font style="font-family:Phetsarath OT"><?=$lang_select?></font></option>
					<option ng-repeat="x in zonelist" value="{{x.zone_id}}">
						<font style="font-family:Phetsarath OT">{{x.zone_name}}</font>
					</option>
				</select>

	<p></p>
		<font style="font-family:Phetsarath OT"><?=$lang_alertwhennum?></font>
		<input type="text" name="product_num_min" id="product_num_min"  placeholder="0" class="form-control text-right">

		<p></p>

<button class="btn btn-success" type="submit"><font style="font-family:Phetsarath OT">
	<?=$lang_save?></font></button>
</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><font style="font-family:Phetsarath OT">Close</font></button>

			</div>
		</div>
	</div>
</div>




<div class="modal fade" id="Modalexcel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><font style="font-family:Phetsarath OT">
					<?=$lang_productlistfromexcel?></font></h4>
			</div>
			<div class="modal-body text-center">

<form enctype="multipart/form-data" id="formexcel">
<input type="file" accept=".csv" id="excel" name="excel" class="btn btn-default">
<br />
<button class="btn btn-success" id="submitexcel" type="submit"><font style="font-family:Phetsarath OT">
	<?=$lang_upload?></font></button>
</form>

<hr />
<font color="red"><font style="font-family:Phetsarath OT"><?=$lang_csvexsample?></font>
<br />
<b><font style="font-family:Phetsarath OT">Use  OpenOffice Calc</font></b>
</font>
<br />
<img src="<?php echo $base_url;?>/pic/imcsv.png">
			</div>

		</div>
	</div>
</div>







	</div>


	</div>

	</div>
</font>

	<script>
var app = angular.module('firstapp', []);
app.controller('Index', function($scope,$http,$location) {



	$("#product_date_end").datetimepicker({
	    timepicker:false,
	        format:'d-m-Y',
	    lang:'th'  // แสดงภาษาไทย
	    //yearOffset:543  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
	    //inline:true

	});

	$("#product_date_end2").datetimepicker({
	    timepicker:false,
	        format:'d-m-Y',
	    lang:'th'  // แสดงภาษาไทย
	    //yearOffset:543  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
	    //inline:true

	});





$scope.product_unit_id = '0';

$scope.product_category_id = '0';
$scope.supplier_id = '0';
$scope.zone_id = '0';
$scope.productlist = [];

$scope.Modalexcel = function(){
$('#Modalexcel').modal('show');
};

$scope.Modaladd = function(){
$('#Openadd').modal('show');
};



$scope.getcategory = function(){

$http.get('Productcategory/get')
       .then(function(response){
          $scope.categorylist = response.data.list;

        });
   };
$scope.getcategory();


$scope.getsupplier = function(){

$http.get('Supplier/getlist')
       .then(function(response){
          $scope.supplierlist = response.data.list;

        });
   };
$scope.getsupplier();



$scope.getunit = function(){

$http.get('Productunit/get')
       .then(function(response){
          $scope.unitlist = response.data.list;

        });
   };
$scope.getunit();





$scope.getzone = function(){

$http.get('Zone/getlist')
       .then(function(response){
          $scope.zonelist = response.data.list;

        });
   };
$scope.getzone();



$scope.perpage = '10';
$scope.getlist = function(searchtext,page,perpage){
    if(!searchtext){
   	searchtext = '';
   }


    if(!page){
   var	page = '1';
   }

 if(!perpage){
   var	perpage = '10';
   }

 $http.post("Productlist/get",{
searchtext:searchtext,
page: page,
perpage: perpage
}).success(function(data){
          $scope.list = data.list;
                 $scope.pageall = data.pageall;
$scope.numall = data.numall;

$scope.pagealladd = [];
           for(i=1;i<=$scope.pageall;i++){
$scope.pagealladd.push({a:i});
}

$scope.selectpage = page;
$scope.selectthispage = page;
        });
   };
$scope.getlist('','1');





$scope.Saveproduct = function(product_code,product_name,product_price,product_pricebase,product_category_id,supplier_id,product_score){
$http.post("Productlist/Add",{
	product_code: product_code,
	product_name: product_name,
	product_price: product_price,
	product_pricebase: product_pricebase,
	product_category_id: product_category_id,
	product_score: product_score,
	supplier_id: supplier_id
	}).success(function(data){
toastr.success('<?=$lang_success?>');
$scope.product_code = '';
$scope.product_name = '';
$scope.product_pricebase = '';
$scope.product_price = '';
$scope.product_score = '';
$scope.getlist();
        });
};



$(document).ready(function (e) {
    $('#uploadImg').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: 'Productlist/Add',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){

	if(data=='1'){
		toastr.warning('ລະຫັດສິນຄ້າ/ບາໂຄດ ຊ້ຳ');
	}else{
		toastr.success('ສຳເລັດ');
$( "#uploadImg" )[0].reset();
$('#Openadd').modal('hide');
$scope.getlist();
}

            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));


});



$scope.Editinputproduct = function(x){
	$('#Openedit').modal('show');
$("#product_id").val(x.product_id);
$("#product_code").val(x.product_code);
$("#product_name").val(x.product_name);
$("#product_date_end2").val(x.product_date_end);
$("#product_des").val(x.product_des);
$("#product_image2").val(x.product_image);
$("#product_price").val(x.product_price);
$("#product_wholesale_price").val(x.product_wholesale_price);
$("#product_pricebase").val(x.product_pricebase);
$("#product_category_id").val(x.product_category_id);
$("#product_unit_id").val(x.product_unit_id);
$("#product_score").val(x.product_score);
$("#zone_id").val(x.zone_id);
$("#supplier_id").val(x.supplier_id);
$("#product_num_min").val(x.product_num_min);

$scope.product_image = x.product_image;

};

$scope.Cancelproduct = function(product_id){
$scope.product_id = '';
$scope.getlist();
};

/*$scope.Editsaveproduct = function(product_id,product_code,product_name,product_price,product_pricebase,product_category_id,supplier_id){
$http.post("Productlist/Update",{
	product_id: product_id,
	product_code: product_code,
	product_name: product_name,
	product_pricebase: product_pricebase,
	product_price: product_price,
	product_category_id: product_category_id,
	supplier_id: supplier_id
	}).success(function(data){
toastr.success('บันทึกเรียบร้อย');
$scope.product_id = '';
$scope.getlist();

        });
};*/



$(document).ready(function (e) {
    $('#Updatedata').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: 'Productlist/Update',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){

							if(data=='1'){
								toastr.warning('ລະຫັດສິນຄ້າ/ບາໂຄດ ຊ້ຳ');
							}else{
								toastr.success('ສຳເລັດ');
$( "#Updatedata" )[0].reset();
$scope.getlist($scope.searchtext,$scope.selectthispage,$scope.perpage);
$('#Openedit').modal('hide');
}



            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

});





$scope.Deleteproduct = function(product_id){
$http.post("Productlist/Delete",{
	product_id: product_id
	}).success(function(data){
toastr.success('<?=$lang_success?>');
$scope.getlist();
        });
};






    $("form#formexcel").submit(function () {
var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: "Productlist/uploadexcel",
            data:formData,
            processData: false,
   		 	contentType: false,
            success: function () {
               toastr.success('<?=$lang_success?>');
               $('#Modalexcel').modal('hide');
               $scope.getlist('','1');
            }
        });
    });







$scope.Updatematmodal = function(x){
$('#updatematmodal').modal('show');
$scope.matdata = x;
}



$scope.Updatematok = function(){
$http.post("Stock/Updatematok",{
product_id: $scope.matdata.product_id,
product_stock_num: $scope.matdata.product_stock_num
}).success(function(data){
	$scope.getlist($scope.searchtext,$scope.selectthispage,$scope.perpage);
	$('#updatematmodal').modal('hide');
});


}







$scope.Updatepotmodal = function(x){
$('#updatepotmodal').modal('show');
$scope.potdata = x;
$http.post("Productlist/getpotlist",{
product_id: x.product_id
}).success(function(data){

$scope.getproductpotlist = data;

});

}



$scope.Addpot = function(x){
$http.post("Productlist/addpot",{
product_id: $scope.potdata.product_id,
product_name: $scope.potdata.product_name,
product_unit_name: $scope.potdata.product_unit_name,
product_id_relation: x.product_id,
product_num_relation: x.product_num_relation,
product_name_relation: x.product_name,
product_unit_relation: x.product_unit_name
}).success(function(data){


	$http.post("Productlist/getpotlist",{
	product_id: $scope.potdata.product_id
	}).success(function(data){

	$scope.getproductpotlist = data;

	});



});

}



$scope.Delpot = function(x){
$http.post("Productlist/delpot",{
prl_ID: x.prl_ID
}).success(function(data){


	$http.post("Productlist/getpotlist",{
	product_id: $scope.potdata.product_id
	}).success(function(data){

	$scope.getproductpotlist = data;

	});


});

}






$scope.Searchpot = function(s){
$http.post("Productlist/searchpot",{
searchtext: s
}).success(function(data){
$scope.getpotlist = data;
});

}





});
	</script>
