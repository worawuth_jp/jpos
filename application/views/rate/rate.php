
<div class="col-md-10 col-sm-9" ng-app="firstapp" ng-controller="Index">

<div class="panel panel-default">
	<div class="panel-body">


<div style="float: right;">
	<input type="checkbox" ng-model="showdeletcbut"> <font style="font-family:Phetsarath OT"><?=$lang_showdel?></font>
</div>
<table id="headerTable" class="table table-hover table-bordered">
	<thead>
		<tr style="background-color: #eee;">
			<th style="width: 50px;"><font style="font-family:Phetsarath OT"><?=$lang_rank?></font></th>
			<th><font style="font-family:Phetsarath OT"><?=$lang_money_currency?></font></th>
			<th style="width: 120px;"><font style="font-family:Phetsarath OT"><?=$lang_manage?></font></th>
		</tr>
	</thead>
	<tbody>
	<tr>
	<td></td>
			<td><input type="text" class="form-control" placeholder="<?=$lang_money_currency?>" ng-model="rate"></td>
			<td><button class="btn btn-success" ng-click="Saverate(rate)">
				<font style="font-family:Phetsarath OT"><?=$lang_save?></font></button></td>
	</tr>

		<tr ng-repeat="x in ratelist">

		<td align="center"><font style="font-family:Phetsarath OT">{{$index+1}}</font></td>

			<td ng-show="curid==x.curid"><input type="text" ng-model="x.rate" class="form-control"></td>

			<td ng-show="curid!=x.curid"><font style="font-family:Phetsarath OT">{{x.rate}}</font></td>

			<td ng-show="curid!=x.curid">

				<button class="btn btn-xs btn-warning" ng-click="Editrate(x.curid)"><font style="font-family:Phetsarath OT"><?=$lang_edit?></font></button>
				<button  ng-show="showdeletcbut" class="btn btn-xs btn-danger" ng-click="Deleterate(x.curid)">
				<font style="font-family:Phetsarath OT"><?=$lang_delete?></font></button>
			</td>

			<td ng-show="curid==x.curid">

				<button class="btn btn-xs btn-success" ng-click="Editsaverate(x.curid,x.rate)">
				<font style="font-family:Phetsarath OT"><?=$lang_save?></font></button>
				<button class="btn btn-xs btn-default" ng-click="Cancelrate(x.currencyid)">
					<font style="font-family:Phetsarath OT"><?=$lang_cancel?></font></button>
			</td>

		</tr>
	</tbody>
</table>


<hr />
<button id="btnExport" class="btn btn-default" onclick="fnExcelReport();"> <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
<font style="font-family:Phetsarath OT"><?=$lang_downloadexcel?></font> </button>

	</div>


	</div>

	</div>


	<script>
var app = angular.module('firstapp', []);
app.controller('Index', function($scope,$http,$location) {

$scope.get = function(){

$http.get('Rate/get')
       .then(function(response){
          $scope.ratelist = response.data.list;

        });
   };
$scope.get();

$scope.Saverate = function(rate){
$http.post("Rate/Add",{
	rate: rate
	}).success(function(data){
toastr.success('<?=$lang_success?>');
$scope.get();
        });
};

$scope.Editrate = function(curid){
$scope.curid = curid;
};

$scope.Cancelrate = function(curid){
$scope.curid = '';
$scope.get();
};

$scope.Editsaverate = function(curid,rate){
$http.post("Rate/Update",{
	curid: curid,
	rate: rate
	}).success(function(data){
toastr.success('<?=$lang_success?>');
$scope.curid = '';
$scope.get();

        });
};


$scope.Deleterate = function(curid){
$http.post("Rate/Delete",{
	curid: curid
	}).success(function(data){
toastr.success('<?=$lang_success?>');
$scope.get();
        });
};




});
	</script>
