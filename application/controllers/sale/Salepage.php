<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salepage extends MY_Controller {


 public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('sale/salepage_model');

     if(!isset($_SESSION['owner_id'])){
            header( "location: ".$this->base_url );
        }

    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{


$data['tab'] = 'salepage';
$data['title'] = 'Sale Page';
		$this->deshboardlayout('sale/salepage',$data);
}


//================== box product for sale============================================

function Getproductlist()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->salepage_model->Getproductlist($data);

	}



//======================

function Getrate()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->ratenow_model->Getrate($data);

  }

//======================


	function Findproduct()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->salepage_model->Findproduct($data);

	}


// ============= show customer data ====================================================
function Customer()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->salepage_model->Customer($data);

	}

//=================================================================

  function Getquotation()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->salepage_model->Getquotation($data);

	}




	function Gettoday()
    {

$data = json_decode(file_get_contents("php://input"),true);
echo  $this->salepage_model->Gettoday($data);

	}




	function Updatemoneychange()
    {

$this->salepage_model->Updatemoneychange();

	}




function Savesale()
    {

	$data = json_decode(file_get_contents("php://input"),true);
if(!isset($data)){
exit();
}

$numforcuslast = $this->salepage_model->Getnumforcuslast();
$numforcusnow = $numforcuslast[0]['number_for_cus'];

$numforcusplus = $numforcusnow + 1;


$runnolast = $this->salepage_model->Getrunnolast();


$runnonow = $runnolast[0]['sale_runno'];

//$runnonow2 = substr($runnonow,-$_SESSION['runno_digit']);
//$runnoplus = $runnonow2 + 1;
//$header_code = $_SESSION['befor_runno'].str_pad($runnoplus, $_SESSION['runno_digit'], "0", STR_PAD_LEFT);
$runnoplus = $runnonow + 1;
$header_code = str_pad($runnoplus, $_SESSION['runno_digit'], "0", STR_PAD_LEFT);


$adddate = time();
//$header_code = time();

for($i=1;$i<=count($data['listsale']) ;$i++){

$data['number_for_cus'] = $numforcusplus;

$data['sale_runno'] = $header_code;
$data['adddate'] = $adddate;

	if($data['listsale'][$i-1]['product_id']!='' && $data['listsale'][$i-1]['product_sale_num']!='0'){
$data['listsale'][$i-1]['sale_runno'] = $header_code;
$data['listsale'][$i-1]['adddate'] = $adddate;

$data['listsale'][$i-1]['ID'] = null;

$this->salepage_model->Adddetail($data['listsale'][$i-1]);
	$this->salepage_model->Updateproductdeletestock($data['listsale'][$i-1]);


  $getrelationlist = $this->salepage_model->Getrelationlist($data['listsale'][$i-1]['product_id']);

  //print_r($getrelationlist);
  foreach ($getrelationlist as $key => $value) {
  $this->salepage_model->Updateproductdeletestock_relation($value['product_id_relation'],($value['product_num_relation']*$data['listsale'][$i-1]['product_sale_num']));

  }







if($i==count($data['listsale'])){
$this->salepage_model->Addheader($data);
$price_value = $data['sumsale_price']-$data['discount_last'];
$this->salepage_model->Addmoneychange($data['money_changeto_customer'],$data['money_from_customer'],$price_value);
}

}

}



	}
























  function Savequotation()
      {

  	$data = json_decode(file_get_contents("php://input"),true);
  if(!isset($data)){
  exit();
  }

  $numforcuslast = $this->salepage_model->Getnumforcuslast();
  $numforcusnow = $numforcuslast[0]['number_for_cus'];

  $numforcusplus = $numforcusnow + '1';


  //$runnolast = $this->salepage_model->Getrunnolast();



  $adddate = time();
  $header_code = 'quo'.time();

  for($i=1;$i<=count($data['listsale']) ;$i++){

  $data['number_for_cus'] = $numforcusplus;

  $data['sale_runno'] = $header_code;
  $data['adddate'] = $adddate;



  	if($data['listsale'][$i-1]['product_id']!='' && $data['listsale'][$i-1]['product_sale_num']!='0'){
  $data['listsale'][$i-1]['sale_runno'] = $header_code;
  $data['listsale'][$i-1]['adddate'] = $adddate;

$data['listsale'][$i-1]['ID'] = null;

  if($this->salepage_model->Adddetailquotation($data['listsale'][$i-1])){
  	//$this->salepage_model->Updateproductdeletestock($data['listsale'][$i-1]);


    //$getrelationlist = $this->salepage_model->Getrelationlist($data['listsale'][$i-1]['product_id']);

    //print_r($getrelationlist);
    //foreach ($getrelationlist as $key => $value) {
    //$this->salepage_model->Updateproductdeletestock_relation($value['product_id_relation'],($value['product_num_relation']*$data['listsale'][$i-1]['product_sale_num']));

    //}

  }





  if($i==1){
  $this->salepage_model->Addheaderquotation($data);
  //$price_value = $data['sumsale_price']-$data['discount_last'];
  //$this->salepage_model->Addmoneychange($data['money_changeto_customer'],$data['money_from_customer'],$price_value);
  }

  }

  }



  	}






	}
