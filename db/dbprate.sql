-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 18, 2020 at 01:12 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minimart_rate`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcode_ds`
--

CREATE TABLE `barcode_ds` (
  `ds_id` int(11) NOT NULL,
  `widthlabel` varchar(255) NOT NULL,
  `heightlabel` varchar(255) NOT NULL,
  `marginright` varchar(255) NOT NULL,
  `marginbottom` varchar(255) NOT NULL,
  `heightbarcode` varchar(255) NOT NULL,
  `widthbarcode` varchar(255) NOT NULL,
  `sizeprice` varchar(255) NOT NULL,
  `sizepname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barcode_ds`
--

INSERT INTO `barcode_ds` (`ds_id`, `widthlabel`, `heightlabel`, `marginright`, `marginbottom`, `heightbarcode`, `widthbarcode`, `sizeprice`, `sizepname`) VALUES
(1, '160', '', '10', '10', '', '', '12', '10');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `contact_des` longtext NOT NULL,
  `add_time` varchar(100) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_from`
--

CREATE TABLE `contact_from` (
  `contact_from_id` int(11) NOT NULL,
  `contact_from_name` varchar(255) NOT NULL,
  `contact_from_remark` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_grade`
--

CREATE TABLE `contact_grade` (
  `contact_grade_id` int(11) NOT NULL,
  `contact_grade_name` varchar(255) NOT NULL,
  `contact_grade_remark` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_list`
--

CREATE TABLE `contact_list` (
  `contact_list_id` int(11) NOT NULL,
  `contact_list_detail` text NOT NULL,
  `contact_from_id` int(11) NOT NULL,
  `contact_grade_id` int(11) NOT NULL,
  `product_service_id` int(11) NOT NULL,
  `customer_reasonbuy_id` int(11) NOT NULL,
  `customer_reasonnotbuy_id` int(11) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `addtime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_center`
--

CREATE TABLE `customer_center` (
  `cus_cen_id` int(11) NOT NULL,
  `cus_cen_name` varchar(100) NOT NULL,
  `cus_cen_tel` int(10) NOT NULL,
  `cus_cen_email` varchar(100) NOT NULL,
  `cus_cen_add_time` varchar(100) NOT NULL,
  `cus_cen_code` varchar(100) NOT NULL,
  `type_bu_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE `customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `customer_group_name` varchar(255) NOT NULL,
  `customer_group_remark` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_level`
--

CREATE TABLE `customer_level` (
  `customer_level_id` int(11) NOT NULL,
  `customer_level_name` varchar(255) NOT NULL,
  `customer_level_remark` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_owner`
--

CREATE TABLE `customer_owner` (
  `cus_id` int(11) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_address` varchar(255) NOT NULL,
  `cus_tel` varchar(15) NOT NULL,
  `cus_email` varchar(100) NOT NULL,
  `cus_birthday` varchar(20) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `customer_level_id` int(11) NOT NULL,
  `customer_sex_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `amphur_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `cus_address_postcode` varchar(5) NOT NULL,
  `cus_remark` varchar(255) NOT NULL,
  `product_score_all` decimal(65,2) NOT NULL,
  `cus_add_time` varchar(100) NOT NULL,
  `cus_edit_time` varchar(100) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reasonbuy`
--

CREATE TABLE `customer_reasonbuy` (
  `customer_reasonbuy_id` int(11) NOT NULL,
  `customer_reasonbuy_name` varchar(255) NOT NULL,
  `customer_reasonbuy_remark` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reasonnotbuy`
--

CREATE TABLE `customer_reasonnotbuy` (
  `customer_reasonnotbuy_id` int(11) NOT NULL,
  `customer_reasonnotbuy_name` varchar(255) NOT NULL,
  `customer_reasonnotbuy_remark` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_sex`
--

CREATE TABLE `customer_sex` (
  `customer_sex_id` int(11) NOT NULL,
  `customer_sex_name` varchar(255) NOT NULL,
  `customer_sex_remark` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `money_rate`
--

CREATE TABLE `money_rate` (
  `curid` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `money_rate`
--

INSERT INTO `money_rate` (`curid`, `rate`, `owner_id`, `user_id`) VALUES
(3, '380.00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `owner_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `owner_logo` varchar(255) NOT NULL,
  `owner_bgimg` varchar(255) NOT NULL,
  `owner_name` varchar(255) NOT NULL,
  `owner_address` varchar(255) NOT NULL,
  `owner_tax_number` varchar(100) NOT NULL,
  `owner_vat` int(2) NOT NULL,
  `owner_vat_status` int(1) NOT NULL,
  `province_id` int(11) NOT NULL,
  `amphur_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `postcode` int(5) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `add_time` varchar(100) NOT NULL,
  `end_time` varchar(100) NOT NULL,
  `owner_type_id` int(11) NOT NULL,
  `owner_email` varchar(100) NOT NULL,
  `owner_pass` varchar(100) NOT NULL,
  `type_bu_id` int(3) NOT NULL,
  `owner_money` int(100) NOT NULL,
  `status_pay` int(1) NOT NULL,
  `aff_id` int(10) NOT NULL,
  `aff_tag` varchar(20) NOT NULL,
  `aff_income` int(10) NOT NULL,
  `cashier_printer_ip` varchar(255) NOT NULL,
  `printer_type` int(1) NOT NULL COMMENT '1=55mm, 2=80mm',
  `printer_ul` int(1) NOT NULL COMMENT '0=usbonline,1=usboffline,2=lannetwork',
  `printer_name` varchar(255) NOT NULL,
  `money_change_showcus` decimal(65,2) NOT NULL,
  `money_from_cus_showcus` varchar(255) NOT NULL,
  `price_value_showcus` varchar(255) NOT NULL,
  `number_for_cus` int(11) NOT NULL,
  `footer_slip` text NOT NULL,
  `header_slip` text NOT NULL,
  `footer_a4` text NOT NULL,
  `header_a4` text NOT NULL,
  `befor_runno` varchar(255) NOT NULL,
  `runno_digit` int(11) NOT NULL,
  `reset_runno` int(1) NOT NULL,
  `youtube_url_forcus` varchar(255) NOT NULL,
  `ads` int(1) NOT NULL,
  `color_theme` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`owner_id`, `store_id`, `owner_logo`, `owner_bgimg`, `owner_name`, `owner_address`, `owner_tax_number`, `owner_vat`, `owner_vat_status`, `province_id`, `amphur_id`, `district_id`, `postcode`, `tel`, `add_time`, `end_time`, `owner_type_id`, `owner_email`, `owner_pass`, `type_bu_id`, `owner_money`, `status_pay`, `aff_id`, `aff_tag`, `aff_income`, `cashier_printer_ip`, `printer_type`, `printer_ul`, `printer_name`, `money_change_showcus`, `money_from_cus_showcus`, `price_value_showcus`, `number_for_cus`, `footer_slip`, `header_slip`, `footer_a4`, `header_a4`, `befor_runno`, `runno_digit`, `reset_runno`, `youtube_url_forcus`, `ads`, `color_theme`) VALUES
(1, 1, '', 'pic/bg/1/1575462979e775d60cf37126d299d69563241d7787.jpg', 'test', 'test', '', 0, 0, 0, 0, 0, 0, '99999999', '07-03-2018', '19-03-2018', 0, '0', '0', 0, 0, 0, 0, '', 0, '192.168.0.10', 1, 0, 'ssss', '0.01', '39000', '39000', 141, 'ຂອບໃຈທີ່ໃຊ້ບໍລິການ', 'ໃບຮັບເງິນ', 'ຂອບໃຈທີ່ໃຊ້ບໍລິການ', 'ໃບບິນຮັບເງິນ/ໃບສົ່ງສິນຄ້າ', 'INV', 10, 0, 'https://www.youtube.com/watch?v=a9RZNEUx-b8', 1, '#c0c0c0');

-- --------------------------------------------------------

--
-- Table structure for table `pawn`
--

CREATE TABLE `pawn` (
  `pawn_id` int(11) NOT NULL,
  `cus_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cus_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cus_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cus_tel` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_sn` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pawn_money` decimal(65,2) NOT NULL,
  `pawn_percent` int(11) NOT NULL,
  `end_date` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pawn_status` int(1) NOT NULL,
  `add_time` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pawnadddate`
--

CREATE TABLE `pawnadddate` (
  `p_id` int(11) NOT NULL,
  `pawn_id` int(11) NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_sn` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pawnadd_cutmoney` decimal(65,2) NOT NULL,
  `pawnadd_permoney` decimal(65,2) NOT NULL,
  `pawnadd_adddate` varchar(255) NOT NULL,
  `add_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_price_cus`
--

CREATE TABLE `product_price_cus` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `product_price_cus` decimal(65,2) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_price_cus_group`
--

CREATE TABLE `product_price_cus_group` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `product_price_cus_group` decimal(65,2) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_price_step`
--

CREATE TABLE `product_price_step` (
  `ps_ID` int(11) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `qty_more` int(11) NOT NULL,
  `qty_less` int(11) NOT NULL,
  `product_price` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_return_datail`
--

CREATE TABLE `product_return_datail` (
  `ID` int(11) NOT NULL,
  `return_runno` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` int(255) NOT NULL,
  `product_sale_num` int(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_return_header`
--

CREATE TABLE `product_return_header` (
  `ID` int(11) NOT NULL,
  `return_runno` varchar(255) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address_all` text NOT NULL,
  `sumsale_discount` decimal(65,2) NOT NULL,
  `sumsale_num` int(255) NOT NULL,
  `sumsale_price` decimal(65,2) NOT NULL,
  `money_from_customer` decimal(65,2) NOT NULL,
  `money_changeto_customer` decimal(65,2) NOT NULL,
  `vat` int(2) NOT NULL,
  `product_score_all` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_service`
--

CREATE TABLE `product_service` (
  `product_service_id` int(11) NOT NULL,
  `product_service_name` varchar(255) NOT NULL,
  `product_service_price` int(11) NOT NULL,
  `product_service_remark` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_buy_detail`
--

CREATE TABLE `purchase_buy_detail` (
  `importproduct_detail_id` int(11) NOT NULL,
  `importproduct_header_code` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `importproduct_detail_pricebase` decimal(65,2) NOT NULL,
  `importproduct_detail_num` int(11) NOT NULL,
  `price_per_num` decimal(65,2) NOT NULL,
  `importproduct_detail_date` varchar(255) NOT NULL,
  `lotno` varchar(255) NOT NULL,
  `date_end` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_buy_detail`
--

INSERT INTO `purchase_buy_detail` (`importproduct_detail_id`, `importproduct_header_code`, `product_id`, `importproduct_detail_pricebase`, `importproduct_detail_num`, `price_per_num`, `importproduct_detail_date`, `lotno`, `date_end`, `owner_id`, `user_id`, `store_id`) VALUES
(1, 'PN1576154927', 5, '0.00', 50, '9000000.00', '1576154927', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_buy_header`
--

CREATE TABLE `purchase_buy_header` (
  `importproduct_header_id` int(11) NOT NULL,
  `importproduct_header_code` varchar(255) NOT NULL,
  `importproduct_header_refcode` varchar(50) NOT NULL,
  `importproduct_header_num` int(11) NOT NULL,
  `allprice` decimal(65,2) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `vendor_name` text NOT NULL,
  `importproduct_header_amount` decimal(65,2) NOT NULL,
  `importproduct_header_remark` varchar(255) NOT NULL,
  `importproduct_header_date` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=new, 1=instock'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_buy_header`
--

INSERT INTO `purchase_buy_header` (`importproduct_header_id`, `importproduct_header_code`, `importproduct_header_refcode`, `importproduct_header_num`, `allprice`, `vendor_id`, `vendor_name`, `importproduct_header_amount`, `importproduct_header_remark`, `importproduct_header_date`, `owner_id`, `user_id`, `store_id`, `status`) VALUES
(1, 'PN1576154927', '', 50, '450000000.00', 0, '', '0.00', '', '1576154927', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quotation_list_datail`
--

CREATE TABLE `quotation_list_datail` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_unit_name` varchar(255) NOT NULL,
  `product_des` longtext NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` decimal(65,2) NOT NULL,
  `product_sale_num` varchar(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_price_discount_percent` decimal(65,0) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `sc_ID` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation_list_datail`
--

INSERT INTO `quotation_list_datail` (`ID`, `sale_runno`, `product_id`, `product_name`, `product_image`, `product_unit_name`, `product_des`, `product_code`, `product_price`, `product_sale_num`, `product_price_discount`, `product_price_discount_percent`, `product_score`, `adddate`, `owner_id`, `user_id`, `store_id`, `sc_ID`, `shift_id`) VALUES
(1, 'quo1575269874', 1, 'beer', '', '', '', '1', '10000.00', '1', '0.00', '0', 0, '1575269874', 1, 1, 1, 2, 1),
(2, 'quo1575269901', 1, 'beer', '', '', '', '1', '10000.00', '1', '0.00', '0', 0, '1575269901', 1, 1, 1, 3, 1),
(4, 'quo1575271673', 1, 'beer', '', '', '', '1', '10000.00', '2', '0.00', '0', 0, '1575271673', 1, 1, 1, 1, 1),
(5, 'quo1596339367', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1596339367', 1, 1, 1, 15, 1),
(6, 'quo1596339385', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1596339385', 1, 1, 1, 16, 1),
(7, 'quo1599548106', 37, 'ຊັ້ນໃສ່ວາງເຄື່ອງ4 ຊັ້ນລຸ້ນໜາຕາຖີ່', '', '', '', '37', '120000.00', '1', '0.00', '0', 0, '1599548106', 1, 1, 1, 60, 1),
(8, 'quo1599548106', 5, 'ມີດສອງຄົມດ້າມເຫຼືອງ', '', '', '', '5', '6000.00', '2', '0.00', '0', 0, '1599548106', 1, 1, 1, 61, 1),
(9, 'quo1599548106', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599548106', 1, 1, 1, 62, 1),
(10, 'quo1599548106', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599548106', 1, 1, 1, 63, 1),
(11, 'quo1599548106', 195, 'ສະກ໋ອດໃສກໍ້ໃຫຍ່', '', '', '', '1576408627', '7000.00', '1', '0.00', '0', 0, '1599548106', 1, 1, 1, 64, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quotation_list_header`
--

CREATE TABLE `quotation_list_header` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address_all` text NOT NULL,
  `sumsale_discount` decimal(65,2) NOT NULL,
  `sumsale_num` varchar(255) NOT NULL,
  `sumsale_price` decimal(65,2) NOT NULL,
  `money_from_customer` decimal(65,2) NOT NULL,
  `money_changeto_customer` decimal(65,2) NOT NULL,
  `vat` int(2) NOT NULL,
  `product_score_all` int(11) NOT NULL,
  `sale_type` int(1) NOT NULL COMMENT '1=ขายปลีก,2=ขายส่ง',
  `pay_type` int(1) NOT NULL COMMENT '1=เงินสด,2=โอน,3=บัตรเครดิต,4=ค้างชำระ',
  `reserv` int(1) NOT NULL COMMENT '1=จอง',
  `discount_last` decimal(65,2) NOT NULL COMMENT 'ส่วนลดท้ายบิล',
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `number_for_cus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation_list_header`
--

INSERT INTO `quotation_list_header` (`ID`, `sale_runno`, `cus_name`, `cus_id`, `cus_address_all`, `sumsale_discount`, `sumsale_num`, `sumsale_price`, `money_from_customer`, `money_changeto_customer`, `vat`, `product_score_all`, `sale_type`, `pay_type`, `reserv`, `discount_last`, `adddate`, `owner_id`, `user_id`, `store_id`, `shift_id`, `number_for_cus`) VALUES
(1, 'quo1575269874', '', 0, '', '0.00', '1', '10000.00', '0.00', '-10000.00', 0, 0, 1, 4, 0, '0.00', '1575269874', 1, 1, 1, 1, 16),
(2, 'quo1575269901', '', 0, '', '0.00', '1', '10000.00', '0.00', '-10000.00', 0, 0, 1, 4, 0, '0.00', '1575269901', 1, 1, 1, 1, 16),
(4, 'quo1575271673', '', 0, '', '0.00', '2', '20000.00', '0.00', '-20000.00', 0, 0, 1, 5, 0, '0.00', '1575271673', 1, 1, 1, 1, 16),
(5, 'quo1596339367', '', 0, '', '0.00', '1', '28000.00', '0.00', '-28000.00', 0, 0, 1, 1, 0, '0.00', '1596339367', 1, 1, 1, 1, 105),
(6, 'quo1596339385', '', 0, '', '0.00', '1', '28000.00', '0.00', '-28000.00', 0, 0, 1, 1, 0, '0.00', '1596339385', 1, 1, 1, 1, 105),
(7, 'quo1599548106', '', 0, '', '0.00', '6', '214000.00', '0.00', '-214000.00', 0, 0, 1, 1, 0, '0.00', '1599548106', 1, 1, 1, 1, 133);

-- --------------------------------------------------------

--
-- Table structure for table `sale_list_cus2mer`
--

CREATE TABLE `sale_list_cus2mer` (
  `sc_ID` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_unit_name` varchar(255) NOT NULL,
  `product_des` longtext NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` decimal(65,2) NOT NULL,
  `product_sale_num` varchar(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_price_discount_percent` decimal(65,0) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sale_list_cus2mer`
--

INSERT INTO `sale_list_cus2mer` (`sc_ID`, `product_id`, `product_name`, `product_image`, `product_unit_name`, `product_des`, `product_code`, `product_price`, `product_sale_num`, `product_price_discount`, `product_price_discount_percent`, `product_score`, `adddate`, `owner_id`, `user_id`, `store_id`) VALUES
(70, 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1600420695', 1, 1, 1),
(71, 79, 'ກ່ອງໃສ່ເຄື່ອງຝາລ໋ອກມີລໍ້ກາງ', '', '', '', '79', '40000.00', '1', '0.00', '0', 0, '1600421006', 1, 1, 1),
(72, 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1600421282', 1, 1, 1),
(73, 108, 'ສາດຢາງພາລາ5m', '', '', '', '108', '115000.00', '1', '0.00', '0', 0, '1600424424', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sale_list_datail`
--

CREATE TABLE `sale_list_datail` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_unit_name` varchar(255) NOT NULL,
  `product_des` longtext NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` decimal(65,2) NOT NULL,
  `product_sale_num` varchar(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_price_discount_percent` decimal(65,0) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `sc_ID` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sale_list_datail`
--

INSERT INTO `sale_list_datail` (`ID`, `sale_runno`, `product_id`, `product_name`, `product_image`, `product_unit_name`, `product_des`, `product_code`, `product_price`, `product_sale_num`, `product_price_discount`, `product_price_discount_percent`, `product_score`, `adddate`, `owner_id`, `user_id`, `store_id`, `sc_ID`, `shift_id`) VALUES
(1, '0000000001', 280, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼດ', '', '', '', '1576415316', '13000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 1, 1),
(2, '0000000001', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 2, 1),
(3, '0000000001', 279, 'ຢ່ວງຫ້ອຍເຄື່ອງມົນເຫຼດ', '', '', '', '1576415279', '13000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 3, 1),
(4, '0000000001', 278, 'ກະຕ່າມີຝາ ດ້ານດຽວກາງ', '', '', '', '1576415251', '10000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 4, 1),
(5, '0000000001', 270, 'ໝໍ້ກາຕົ້ມນ້ຳກາງ', '', '', '', '1576415011', '30000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 5, 1),
(6, '0000000001', 269, 'ໝໍ້ກາຕົ້ມນ້ຳນ້ອຍ', '', '', '', '1576414994', '25000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 6, 1),
(7, '0000000001', 268, 'ໝໍ້ຕົ້ມນ້ຳໂອໂຕ 3.8 L', '', '', '', '1576414968', '260000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 7, 1),
(8, '0000000001', 158, 'ຂຽງໄມ້ລາວນ້ອຍ', '', '', '', '1576407177', '20000.00', '1', '0.00', '0', 0, '1578329046', 1, 1, 1, 8, 1),
(9, '0000000002', 274, 'ຟອຍໄທດ້າມຢາງ', '', '', '', '1576415090', '0.00', '2', '0.00', '0', 0, '1589202154', 1, 1, 1, 12, 1),
(10, '0000000002', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589202154', 1, 1, 1, 13, 1),
(11, '0000000003', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589202361', 1, 1, 1, 14, 1),
(12, '0000000004', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1589202546', 1, 1, 1, 15, 1),
(13, '0000000005', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589202662', 1, 1, 1, 16, 1),
(14, '0000000006', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1589202800', 1, 1, 1, 17, 1),
(15, '0000000007', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589202969', 1, 1, 1, 18, 1),
(16, '0000000008', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589203774', 1, 1, 1, 19, 1),
(17, '0000000009', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589204355', 1, 1, 1, 20, 1),
(18, '0000000010', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589204548', 1, 1, 1, 21, 1),
(19, '0000000011', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589204595', 1, 1, 1, 22, 1),
(20, '0000000012', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589204655', 1, 1, 1, 23, 1),
(21, '0000000013', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589204736', 1, 1, 1, 24, 1),
(22, '0000000014', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589204872', 1, 1, 1, 25, 1),
(23, '0000000014', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589204872', 1, 1, 1, 26, 1),
(24, '0000000015', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589205148', 1, 1, 1, 27, 1),
(25, '0000000016', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205199', 1, 1, 1, 28, 1),
(26, '0000000017', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205242', 1, 1, 1, 29, 1),
(27, '0000000018', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589205276', 1, 1, 1, 30, 1),
(28, '0000000019', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589205335', 1, 1, 1, 31, 1),
(29, '0000000019', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205335', 1, 1, 1, 32, 1),
(30, '0000000020', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589205375', 1, 1, 1, 33, 1),
(31, '0000000020', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205375', 1, 1, 1, 34, 1),
(32, '0000000021', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205573', 1, 1, 1, 35, 1),
(33, '0000000021', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589205573', 1, 1, 1, 36, 1),
(34, '0000000022', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205628', 1, 1, 1, 37, 1),
(35, '0000000022', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589205628', 1, 1, 1, 38, 1),
(36, '0000000023', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589205715', 1, 1, 1, 39, 1),
(37, '0000000023', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589205715', 1, 1, 1, 40, 1),
(38, '0000000024', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589206451', 1, 1, 1, 41, 1),
(39, '0000000025', 215, 'ຄັນຫົ່ມໃຫຍ່ຫຼາຍສີ', '', '', '', '1576413168', '115000.00', '1', '0.00', '0', 0, '1589206722', 1, 1, 1, 42, 1),
(40, '0000000026', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589208887', 1, 1, 1, 43, 1),
(41, '0000000027', 37, 'ຊັ້ນໃສ່ວາງເຄື່ອງ4 ຊັ້ນລຸ້ນໜາຕາຖີ່', '', '', '', '37', '120000.00', '1', '0.00', '0', 0, '1589209166', 1, 1, 1, 44, 1),
(42, '0000000028', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589209325', 1, 1, 1, 45, 1),
(43, '0000000029', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589209603', 1, 1, 1, 46, 1),
(44, '0000000030', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589209640', 1, 1, 1, 47, 1),
(45, '0000000031', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589209721', 1, 1, 1, 48, 1),
(46, '0000000032', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589209795', 1, 1, 1, 49, 1),
(47, '0000000033', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589209899', 1, 1, 1, 50, 1),
(48, '0000000034', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589209929', 1, 1, 1, 51, 1),
(49, '0000000035', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589209985', 1, 1, 1, 52, 1),
(50, '0000000036', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210039', 1, 1, 1, 53, 1),
(51, '0000000037', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589210092', 1, 1, 1, 54, 1),
(52, '0000000037', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210092', 1, 1, 1, 55, 1),
(53, '0000000038', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210134', 1, 1, 1, 56, 1),
(54, '0000000039', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210199', 1, 1, 1, 57, 1),
(55, '0000000040', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210269', 1, 1, 1, 58, 1),
(56, '0000000041', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589210787', 1, 1, 1, 59, 1),
(57, '0000000042', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589210817', 1, 1, 1, 60, 1),
(58, '0000000043', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589211343', 1, 1, 1, 61, 1),
(59, '0000000044', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589211378', 1, 1, 1, 62, 1),
(60, '0000000045', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589211431', 1, 1, 1, 63, 1),
(61, '0000000046', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589211470', 1, 1, 1, 64, 1),
(62, '0000000047', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589211635', 1, 1, 1, 65, 1),
(63, '0000000048', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1589211680', 1, 1, 1, 66, 1),
(64, '0000000049', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589211723', 1, 1, 1, 67, 1),
(65, '0000000050', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589211777', 1, 1, 1, 68, 1),
(66, '0000000051', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589211846', 1, 1, 1, 69, 1),
(67, '0000000052', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589211889', 1, 1, 1, 70, 1),
(68, '0000000053', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589211930', 1, 1, 1, 71, 1),
(69, '0000000054', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589212037', 1, 1, 1, 72, 1),
(70, '0000000055', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589212090', 1, 1, 1, 73, 1),
(71, '0000000056', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589212147', 1, 1, 1, 74, 1),
(72, '0000000057', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589212249', 1, 1, 1, 75, 1),
(73, '0000000058', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589212362', 1, 1, 1, 76, 1),
(74, '0000000059', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589212409', 1, 1, 1, 77, 1),
(75, '0000000060', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589212449', 1, 1, 1, 78, 1),
(76, '0000000061', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589212608', 1, 1, 1, 79, 1),
(77, '0000000062', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589212660', 1, 1, 1, 80, 1),
(78, '0000000063', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589212703', 1, 1, 1, 81, 1),
(79, '0000000064', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589212731', 1, 1, 1, 82, 1),
(80, '0000000065', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589212809', 1, 1, 1, 83, 1),
(81, '0000000066', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589212947', 1, 1, 1, 84, 1),
(82, '0000000067', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589213139', 1, 1, 1, 85, 1),
(83, '0000000068', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589214324', 1, 1, 1, 86, 1),
(84, '0000000068', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589214324', 1, 1, 1, 87, 1),
(85, '0000000069', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589214444', 1, 1, 1, 88, 1),
(86, '0000000070', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589214536', 1, 1, 1, 89, 1),
(87, '0000000071', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589214613', 1, 1, 1, 90, 1),
(88, '0000000072', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589214839', 1, 1, 1, 91, 1),
(89, '0000000073', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 92, 1),
(90, '0000000073', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 93, 1),
(91, '0000000073', 215, 'ຄັນຫົ່ມໃຫຍ່ຫຼາຍສີ', '', '', '', '1576413168', '115000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 94, 1),
(92, '0000000073', 214, 'ຄັນຫົ່ມໃຫຍ່ສຸດ', '', '', '', '1576413138', '170000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 95, 1),
(93, '0000000073', 196, 'ສະກ໋ອດໃສກໍ້ນ້ອຍ', '', '', '', '1576408652', '3000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 96, 1),
(94, '0000000073', 195, 'ສະກ໋ອດໃສກໍ້ໃຫຍ່', '', '', '', '1576408627', '7000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 97, 1),
(95, '0000000073', 5, 'ມີດສອງຄົມດ້າມເຫຼືອງ', '', '', '', '5', '6000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 98, 1),
(96, '0000000073', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1589214874', 1, 1, 1, 99, 1),
(97, '0000000074', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 100, 1),
(98, '0000000074', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 101, 1),
(99, '0000000074', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 102, 1),
(100, '0000000074', 215, 'ຄັນຫົ່ມໃຫຍ່ຫຼາຍສີ', '', '', '', '1576413168', '115000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 103, 1),
(101, '0000000074', 214, 'ຄັນຫົ່ມໃຫຍ່ສຸດ', '', '', '', '1576413138', '170000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 104, 1),
(102, '0000000074', 196, 'ສະກ໋ອດໃສກໍ້ນ້ອຍ', '', '', '', '1576408652', '3000.00', '1', '0.00', '0', 0, '1589215019', 1, 1, 1, 105, 1),
(103, '0000000075', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215164', 1, 1, 1, 106, 1),
(104, '0000000075', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215164', 1, 1, 1, 107, 1),
(105, '0000000075', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589215164', 1, 1, 1, 108, 1),
(106, '0000000075', 215, 'ຄັນຫົ່ມໃຫຍ່ຫຼາຍສີ', '', '', '', '1576413168', '115000.00', '1', '0.00', '0', 0, '1589215164', 1, 1, 1, 109, 1),
(107, '0000000075', 214, 'ຄັນຫົ່ມໃຫຍ່ສຸດ', '', '', '', '1576413138', '170000.00', '1', '0.00', '0', 0, '1589215164', 1, 1, 1, 110, 1),
(108, '0000000076', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215233', 1, 1, 1, 111, 1),
(109, '0000000076', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215233', 1, 1, 1, 112, 1),
(110, '0000000076', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589215233', 1, 1, 1, 113, 1),
(111, '0000000077', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215333', 1, 1, 1, 114, 1),
(112, '0000000077', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215333', 1, 1, 1, 115, 1),
(113, '0000000078', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215389', 1, 1, 1, 116, 1),
(114, '0000000078', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215389', 1, 1, 1, 117, 1),
(115, '0000000078', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '1', '0.00', '0', 0, '1589215389', 1, 1, 1, 118, 1),
(116, '0000000079', 214, 'ຄັນຫົ່ມໃຫຍ່ສຸດ', '', '', '', '1576413138', '170000.00', '1', '0.00', '0', 0, '1589215431', 1, 1, 1, 119, 1),
(117, '0000000079', 195, 'ສະກ໋ອດໃສກໍ້ໃຫຍ່', '', '', '', '1576408627', '7000.00', '1', '0.00', '0', 0, '1589215431', 1, 1, 1, 120, 1),
(118, '0000000079', 196, 'ສະກ໋ອດໃສກໍ້ນ້ອຍ', '', '', '', '1576408652', '3000.00', '1', '0.00', '0', 0, '1589215431', 1, 1, 1, 121, 1),
(119, '0000000080', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589215845', 1, 1, 1, 122, 1),
(120, '0000000080', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215845', 1, 1, 1, 123, 1),
(121, '0000000080', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589215845', 1, 1, 1, 124, 1),
(122, '0000000081', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589215971', 1, 1, 1, 125, 1),
(123, '0000000081', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '2', '0.00', '0', 0, '1589215971', 1, 1, 1, 126, 1),
(124, '0000000082', 244, 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '', '', '1576414128', '16000.00', '1', '0.00', '0', 0, '1589216060', 1, 1, 1, 127, 1),
(125, '0000000082', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1589216060', 1, 1, 1, 128, 1),
(126, '0000000082', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1589216060', 1, 1, 1, 129, 1),
(127, '0000000083', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '6', '0.00', '0', 0, '1594515851', 1, 1, 1, 1, 1),
(128, '0000000084', 194, 'ສຳລີຜັນຫູ (1 ກ່ອງ)', '', '', '', '1576408590', '6000.00', '1', '0.00', '0', 0, '1596338286', 1, 1, 1, 13, 1),
(129, '0000000085', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1596339070', 1, 1, 1, 14, 1),
(130, '0000000086', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1596374623', 1, 1, 1, 27, 1),
(131, '0000000086', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1596374623', 1, 1, 1, 28, 1),
(132, '0000000087', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '2', '0.00', '0', 0, '1596385926', 1, 1, 1, 36, 1),
(133, '0000000087', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '2', '0.00', '0', 0, '1596385926', 1, 1, 1, 37, 1),
(134, '0000000087', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1596385926', 1, 1, 1, 38, 1),
(135, '0000000087', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1596385926', 1, 1, 1, 39, 1),
(136, '0000000088', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1597480468', 1, 1, 1, 1, 1),
(137, '0000000089', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1597480548', 1, 1, 1, 2, 1),
(138, '0000000090', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '2', '0.00', '0', 0, '1597490090', 1, 1, 1, 6, 1),
(139, '0000000090', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1597490090', 1, 1, 1, 7, 1),
(140, '0000000091', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1597490150', 1, 1, 1, 8, 1),
(141, '0000000092', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1598006115', 1, 1, 1, 9, 1),
(142, '0000000093', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1598631478', 1, 1, 1, 13, 1),
(143, '0000000094', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '2', '0.00', '0', 0, '1599311078', 1, 1, 1, 16, 1),
(144, '0000000094', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '2', '0.00', '0', 0, '1599311078', 1, 1, 1, 17, 1),
(145, '0000000095', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599311192', 1, 1, 1, 18, 1),
(146, '0000000096', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599311319', 1, 1, 1, 19, 1),
(147, '0000000097', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599311449', 1, 1, 1, 20, 1),
(148, '0000000098', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599311517', 1, 1, 1, 21, 1),
(149, '0000000099', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1599311551', 1, 1, 1, 22, 1),
(150, '0000000100', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599318739', 1, 1, 1, 23, 1),
(151, '0000000101', 5, 'ມີດສອງຄົມດ້າມເຫຼືອງ', '', '', '', '5', '6000.00', '1', '0.00', '0', 0, '1599320368', 1, 1, 1, 24, 1),
(152, '0000000102', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599391642', 1, 1, 1, 30, 1),
(153, '0000000102', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '2', '0.00', '0', 0, '1599391642', 1, 1, 1, 31, 1),
(154, '0000000102', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599391642', 1, 1, 1, 32, 1),
(155, '0000000102', 5, 'ມີດສອງຄົມດ້າມເຫຼືອງ', '', '', '', '5', '6000.00', '1', '0.00', '0', 0, '1599391642', 1, 1, 1, 33, 1),
(156, '0000000103', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599391762', 1, 1, 1, 34, 1),
(157, '0000000104', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599392026', 1, 1, 1, 35, 1),
(158, '0000000105', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '1', '0.00', '0', 0, '1599392064', 1, 1, 1, 36, 1),
(159, '0000000106', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599392115', 1, 1, 1, 37, 1),
(160, '0000000107', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599392217', 1, 1, 1, 38, 1),
(161, '0000000108', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599392246', 1, 1, 1, 39, 1),
(162, '0000000109', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599392305', 1, 1, 1, 40, 1),
(163, '0000000110', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599392345', 1, 1, 1, 41, 1),
(164, '0000000111', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '1', '0.00', '0', 0, '1599392379', 1, 1, 1, 42, 1),
(165, '0000000112', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '1', '0.00', '0', 0, '1599395552', 1, 1, 1, 44, 1),
(166, '0000000112', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '3', '0.00', '0', 0, '1599395552', 1, 1, 1, 45, 1),
(167, '0000000112', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599395552', 1, 1, 1, 46, 1),
(168, '0000000113', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599395955', 1, 1, 1, 47, 1),
(169, '0000000113', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599395955', 1, 1, 1, 48, 1),
(170, '0000000114', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599750983', 1, 1, 1, 3, 1),
(171, '0000000114', 6, 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '6', '35000.00', '1', '0.00', '0', 0, '1599750983', 1, 1, 1, 4, 1),
(172, '0000000115', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1599751279', 1, 1, 1, 5, 1),
(173, '0000000116', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '2', '0.00', '0', 0, '1600085723', 1, 1, 1, 6, 1),
(174, '0000000117', 281, 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '', '', '1576415348', '12000.00', '2', '0.00', '0', 0, '1600085980', 1, 1, 1, 7, 1),
(175, '0000000118', 77, 'ກ່ອງໃສ່ຈອກໃຫຍ່ແປຍາວ', '', '', '', '77', '35000.00', '1', '0.00', '0', 0, '1600086109', 1, 1, 1, 8, 1),
(176, '0000000118', 76, 'ກ່ອງໃສ່ຈອກໃຫຍ່', '', '', '', '76', '35000.00', '2', '0.00', '0', 0, '1600086109', 1, 1, 1, 9, 1),
(177, '0000000118', 75, 'ກ່ອງໃສ່ຖ້ວຍໃຫຍ່ຈຳໂບ້', '', '', '', '75', '95000.00', '1', '0.00', '0', 0, '1600086109', 1, 1, 1, 10, 1),
(178, '0000000118', 79, 'ກ່ອງໃສ່ເຄື່ອງຝາລ໋ອກມີລໍ້ກາງ', '', '', '', '79', '40000.00', '2', '0.00', '0', 0, '1600086109', 1, 1, 1, 11, 1),
(179, '0000000119', 194, 'ສຳລີຜັນຫູ (1 ກ່ອງ)', '', '', '', '1576408590', '6000.00', '4', '0.00', '0', 0, '1600086164', 1, 1, 1, 12, 1),
(180, '0000000119', 190, 'ສໍຂີດມົດແຖມກັບໄຟ', '', '', '', '1576408400', '6000.00', '4', '0.00', '0', 0, '1600086164', 1, 1, 1, 13, 1),
(181, '0000000120', 243, 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '', '', '1576414094', '14000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 14, 1),
(182, '0000000120', 242, '5 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414059', '28000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 15, 1),
(183, '0000000120', 241, '4 ຊັ້ນໃສ່ເກີບ', '', '', '', '1576414043', '28000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 16, 1),
(184, '0000000120', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 17, 1),
(185, '0000000120', 72, 'ກ່ອງໃສ່ຖ້ວຍນ້ອຍຂອງໄທ', '', '', '', '72', '45000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 18, 1),
(186, '0000000120', 67, 'ເຄື່ອງປັ່ນສະແຕນເຫຼດ (ໂຖແກ້ວ)', '', '', '', '67', '230000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 19, 1),
(187, '0000000120', 41, '3ຊັ້ນລຸ້ນຕ່ຳເຫຼັກໃສ່ຈານ', '', '', '', '41', '42000.00', '1', '0.00', '0', 0, '1600086297', 1, 1, 1, 20, 1),
(188, '0000000120', 40, 'ຊັ້ນໃສ່ວາງເຄື່ອງ3 ຊັ້ນລຸ້ນບາງຕາຖີ່', '', '', '', '40', '85000.00', '2', '0.00', '0', 0, '1600086297', 1, 1, 1, 21, 1),
(189, '0000000120', 109, 'ສາດຢາງພາລາ2m', '', '', '', '109', '38000.00', '1', '0.00', '0', 0, '1600086297', 1, 1, 1, 22, 1),
(190, '0000000120', 108, 'ສາດຢາງພາລາ5m', '', '', '', '108', '115000.00', '1', '0.00', '0', 0, '1600086297', 1, 1, 1, 23, 1),
(191, '0000000121', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1600095638', 1, 2, 1, 24, 2),
(192, '0000000122', 224, 'ຄົກໃຫຍ່ສຸດ', '', '', '', '1576413515', '40000.00', '1', '0.00', '0', 0, '1600336624', 1, 2, 1, 52, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sale_list_datail_bak`
--

CREATE TABLE `sale_list_datail_bak` (
  `ID_auto` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_unit_name` varchar(255) NOT NULL,
  `product_des` longtext NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` decimal(65,2) NOT NULL,
  `product_sale_num` varchar(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_price_discount_percent` decimal(65,0) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `sc_ID` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sale_list_header`
--

CREATE TABLE `sale_list_header` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address_all` text NOT NULL,
  `sumsale_discount` decimal(65,2) NOT NULL,
  `sumsale_num` varchar(255) NOT NULL,
  `sumsale_price` decimal(65,2) NOT NULL,
  `money_from_customer` decimal(65,2) NOT NULL,
  `money_changeto_customer` decimal(65,2) NOT NULL,
  `vat` int(2) NOT NULL,
  `product_score_all` int(11) NOT NULL,
  `sale_type` int(1) NOT NULL COMMENT '1=ขายปลีก,2=ขายส่ง',
  `pay_type` int(1) NOT NULL COMMENT '1=เงินสด,2=โอน,3=บัตรเครดิต,4=ค้างชำระ',
  `reserv` int(1) NOT NULL COMMENT '1=จอง',
  `discount_last` decimal(65,2) NOT NULL COMMENT 'ส่วนลดท้ายบิล',
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `number_for_cus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sale_list_header`
--

INSERT INTO `sale_list_header` (`ID`, `sale_runno`, `cus_name`, `cus_id`, `cus_address_all`, `sumsale_discount`, `sumsale_num`, `sumsale_price`, `money_from_customer`, `money_changeto_customer`, `vat`, `product_score_all`, `sale_type`, `pay_type`, `reserv`, `discount_last`, `adddate`, `owner_id`, `user_id`, `store_id`, `shift_id`, `number_for_cus`) VALUES
(1, '0000000001', '', 0, '', '0.00', '8', '383000.00', '383000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1578329046', 1, 1, 1, 1, 20),
(2, '0000000002', '', 0, '', '0.00', '3', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589202154', 1, 1, 1, 1, 21),
(3, '0000000003', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589202361', 1, 1, 1, 1, 22),
(4, '0000000004', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589202546', 1, 1, 1, 1, 23),
(5, '0000000005', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589202662', 1, 1, 1, 1, 24),
(6, '0000000006', '', 0, '', '0.00', '1', '40000.00', '50000.00', '10000.00', 0, 0, 1, 1, 0, '0.00', '1589202800', 1, 1, 1, 1, 25),
(7, '0000000007', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589202969', 1, 1, 1, 1, 26),
(8, '0000000008', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589203774', 1, 1, 1, 1, 27),
(9, '0000000009', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589204355', 1, 1, 1, 1, 28),
(10, '0000000010', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589204548', 1, 1, 1, 1, 29),
(11, '0000000011', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589204595', 1, 1, 1, 1, 30),
(12, '0000000012', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589204655', 1, 1, 1, 1, 31),
(13, '0000000013', '', 0, '', '0.00', '1', '16000.00', '40000.00', '24000.00', 0, 0, 1, 1, 0, '0.00', '1589204736', 1, 1, 1, 1, 32),
(14, '0000000014', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589204872', 1, 1, 1, 1, 33),
(15, '0000000015', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205148', 1, 1, 1, 1, 34),
(16, '0000000016', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205199', 1, 1, 1, 1, 35),
(17, '0000000017', '', 0, '', '0.00', '1', '14000.00', '15000.00', '1000.00', 0, 0, 1, 1, 0, '0.00', '1589205242', 1, 1, 1, 1, 36),
(18, '0000000018', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205276', 1, 1, 1, 1, 37),
(19, '0000000019', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205335', 1, 1, 1, 1, 38),
(20, '0000000020', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205375', 1, 1, 1, 1, 39),
(21, '0000000021', '', 0, '', '0.00', '2', '42000.00', '42000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205573', 1, 1, 1, 1, 40),
(22, '0000000022', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205628', 1, 1, 1, 1, 41),
(23, '0000000023', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589205715', 1, 1, 1, 1, 42),
(24, '0000000024', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589206451', 1, 1, 1, 1, 43),
(25, '0000000025', '', 0, '', '0.00', '1', '115000.00', '115000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589206722', 1, 1, 1, 1, 44),
(26, '0000000026', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589208887', 1, 1, 1, 1, 45),
(27, '0000000027', '', 0, '', '0.00', '1', '120000.00', '120000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209166', 1, 1, 1, 1, 46),
(28, '0000000028', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209325', 1, 1, 1, 1, 47),
(29, '0000000029', '', 0, '', '0.00', '1', '14000.00', '15000.00', '1000.00', 0, 0, 1, 1, 0, '0.00', '1589209603', 1, 1, 1, 1, 48),
(30, '0000000030', '', 0, '', '0.00', '1', '14000.00', '15000.00', '1000.00', 0, 0, 1, 1, 0, '0.00', '1589209640', 1, 1, 1, 1, 49),
(31, '0000000031', '', 0, '', '0.00', '1', '14000.00', '15000.00', '1000.00', 0, 0, 1, 1, 0, '0.00', '1589209721', 1, 1, 1, 1, 50),
(32, '0000000032', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209795', 1, 1, 1, 1, 51),
(33, '0000000033', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209899', 1, 1, 1, 1, 52),
(34, '0000000034', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209929', 1, 1, 1, 1, 53),
(35, '0000000035', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589209985', 1, 1, 1, 1, 54),
(36, '0000000036', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210039', 1, 1, 1, 1, 55),
(37, '0000000037', '', 0, '', '0.00', '2', '30000.00', '30000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210092', 1, 1, 1, 1, 56),
(38, '0000000038', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210134', 1, 1, 1, 1, 57),
(39, '0000000039', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210199', 1, 1, 1, 1, 58),
(40, '0000000040', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210269', 1, 1, 1, 1, 59),
(41, '0000000041', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210787', 1, 1, 1, 1, 60),
(42, '0000000042', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589210817', 1, 1, 1, 1, 61),
(43, '0000000043', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211343', 1, 1, 1, 1, 62),
(44, '0000000044', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211378', 1, 1, 1, 1, 63),
(45, '0000000045', '', 0, '', '0.00', '1', '28000.00', '126000.00', '98000.00', 0, 0, 1, 1, 0, '0.00', '1589211431', 1, 1, 1, 1, 64),
(46, '0000000046', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211470', 1, 1, 1, 1, 65),
(47, '0000000047', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211635', 1, 1, 1, 1, 66),
(48, '0000000048', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211680', 1, 1, 1, 1, 67),
(49, '0000000049', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211723', 1, 1, 1, 1, 68),
(50, '0000000050', '', 0, '', '0.00', '1', '28000.00', '123000.00', '95000.00', 0, 0, 1, 1, 0, '0.00', '1589211777', 1, 1, 1, 1, 69),
(51, '0000000051', '', 0, '', '0.00', '1', '28000.00', '50000.00', '22000.00', 0, 0, 1, 1, 0, '0.00', '1589211846', 1, 1, 1, 1, 70),
(52, '0000000052', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589211889', 1, 1, 1, 1, 71),
(53, '0000000053', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589211930', 1, 1, 1, 1, 72),
(54, '0000000054', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212037', 1, 1, 1, 1, 73),
(55, '0000000055', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212090', 1, 1, 1, 1, 74),
(56, '0000000056', '', 0, '', '0.00', '1', '12000.00', '12000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212147', 1, 1, 1, 1, 75),
(57, '0000000057', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212249', 1, 1, 1, 1, 76),
(58, '0000000058', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212362', 1, 1, 1, 1, 77),
(59, '0000000059', '', 0, '', '0.00', '1', '12000.00', '12000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212409', 1, 1, 1, 1, 78),
(60, '0000000060', '', 0, '', '0.00', '1', '16000.00', '16000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212449', 1, 1, 1, 1, 79),
(61, '0000000061', '', 0, '', '0.00', '1', '28000.00', '50000.00', '22000.00', 0, 0, 1, 1, 0, '0.00', '1589212608', 1, 1, 1, 1, 80),
(62, '0000000062', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589212660', 1, 1, 1, 1, 81),
(63, '0000000063', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212703', 1, 1, 1, 1, 82),
(64, '0000000064', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589212731', 1, 1, 1, 1, 83),
(65, '0000000065', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589212809', 1, 1, 1, 1, 84),
(66, '0000000066', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589212947', 1, 1, 1, 1, 85),
(67, '0000000067', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589213139', 1, 1, 1, 1, 86),
(68, '0000000068', '', 0, '', '0.00', '2', '44000.00', '50000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589214324', 1, 1, 1, 1, 87),
(69, '0000000069', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589214444', 1, 1, 1, 1, 88),
(70, '0000000070', '', 0, '', '0.00', '1', '16000.00', '20000.00', '4000.00', 0, 0, 1, 1, 0, '0.00', '1589214536', 1, 1, 1, 1, 89),
(71, '0000000071', '', 0, '', '0.00', '1', '16000.00', '20000.00', '4000.00', 0, 0, 1, 1, 0, '0.00', '1589214613', 1, 1, 1, 1, 90),
(72, '0000000072', '', 0, '', '0.00', '1', '14000.00', '20000.00', '6000.00', 0, 0, 1, 1, 0, '0.00', '1589214839', 1, 1, 1, 1, 91),
(73, '0000000073', '', 0, '', '0.00', '8', '364000.00', '500000.00', '136000.00', 0, 0, 1, 1, 0, '0.00', '1589214874', 1, 1, 1, 1, 92),
(74, '0000000074', '', 0, '', '0.00', '6', '330000.00', '500000.00', '170000.00', 0, 0, 1, 1, 0, '0.00', '1589215019', 1, 1, 1, 1, 93),
(75, '0000000075', '', 0, '', '0.00', '5', '327000.00', '400000.00', '73000.00', 0, 0, 1, 1, 0, '0.00', '1589215164', 1, 1, 1, 1, 94),
(76, '0000000076', '', 0, '', '0.00', '3', '42000.00', '50000.00', '8000.00', 0, 0, 1, 1, 0, '0.00', '1589215233', 1, 1, 1, 1, 95),
(77, '0000000077', '', 0, '', '0.00', '2', '30000.00', '50000.00', '20000.00', 0, 0, 1, 1, 0, '0.00', '1589215333', 1, 1, 1, 1, 96),
(78, '0000000078', '', 0, '', '0.00', '3', '42000.00', '50000.00', '8000.00', 0, 0, 1, 1, 0, '0.00', '1589215389', 1, 1, 1, 1, 97),
(79, '0000000079', '', 0, '', '0.00', '3', '180000.00', '200000.00', '20000.00', 0, 0, 1, 1, 0, '0.00', '1589215431', 1, 1, 1, 1, 98),
(80, '0000000080', '', 0, '', '0.00', '3', '58000.00', '58000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1589215845', 1, 1, 1, 1, 99),
(81, '0000000081', '', 0, '', '0.00', '3', '46000.00', '50000.00', '4000.00', 0, 0, 1, 1, 0, '0.00', '1589215971', 1, 1, 1, 1, 100),
(82, '0000000082', '', 0, '', '0.00', '3', '58000.00', '60000.00', '2000.00', 0, 0, 1, 1, 0, '0.00', '1589216060', 1, 1, 1, 1, 101),
(83, '0000000083', '', 0, '', '0.00', '6', '72000.00', '72000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1594515851', 1, 1, 1, 1, 102),
(84, '0000000084', '', 0, '', '0.00', '1', '6000.00', '6000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1596338286', 1, 1, 1, 1, 103),
(85, '0000000085', '', 0, '', '0.00', '1', '14000.00', '14000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1596339070', 1, 1, 1, 1, 104),
(86, '0000000086', '', 0, '', '0.00', '2', '54000.00', '540000.00', '486000.00', 0, 0, 1, 1, 0, '0.00', '1596374623', 1, 1, 1, 1, 105),
(87, '0000000087', '', 0, '', '0.00', '6', '178000.00', '178000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1596385926', 1, 1, 1, 1, 106),
(88, '0000000088', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1597480468', 1, 1, 1, 1, 107),
(89, '0000000089', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1597480548', 1, 1, 1, 1, 108),
(90, '0000000090', '', 0, '', '0.00', '3', '115000.00', '115000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1597490090', 1, 1, 1, 1, 109),
(91, '0000000091', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1597490150', 1, 1, 1, 1, 110),
(92, '0000000092', '', 0, '', '0.00', '1', '40000.00', '77777.00', '37777.00', 0, 0, 1, 1, 0, '0.00', '1598006115', 1, 1, 1, 1, 111),
(93, '0000000093', '', 0, '', '0.00', '1', '35000.00', '35000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1598631478', 1, 1, 1, 1, 112),
(94, '0000000094', '', 0, '', '0.00', '4', '126000.00', '200000.00', '74000.00', 0, 0, 1, 1, 0, '0.00', '1599311078', 1, 1, 1, 1, 113),
(95, '0000000095', '', 0, '', '0.00', '1', '35000.00', '38000.00', '3000.00', 0, 0, 1, 1, 0, '0.00', '1599311192', 1, 1, 1, 1, 114),
(96, '0000000096', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599311319', 1, 1, 1, 1, 115),
(97, '0000000097', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599311449', 1, 1, 1, 1, 116),
(98, '0000000098', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599311517', 1, 1, 1, 1, 117),
(99, '0000000099', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599311551', 1, 1, 1, 1, 118),
(100, '0000000100', '', 0, '', '0.00', '1', '35000.00', '35000.00', '350.00', 0, 0, 1, 1, 0, '350.00', '1599318739', 1, 1, 1, 1, 119),
(101, '0000000101', '', 0, '', '0.00', '1', '6000.00', '6000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599320368', 1, 1, 1, 1, 120),
(102, '0000000102', '', 0, '', '0.00', '5', '130000.00', '130000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599391642', 1, 1, 1, 1, 121),
(103, '0000000103', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599391762', 1, 1, 1, 1, 122),
(104, '0000000104', '', 0, '', '0.00', '1', '35000.00', '35000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392026', 1, 1, 1, 1, 123),
(105, '0000000105', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392064', 1, 1, 1, 1, 124),
(106, '0000000106', '', 0, '', '0.00', '1', '40000.00', '50000.00', '10000.00', 0, 0, 1, 1, 0, '0.00', '1599392115', 1, 1, 1, 1, 125),
(107, '0000000107', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392217', 1, 1, 1, 1, 126),
(108, '0000000108', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392246', 1, 1, 1, 1, 127),
(109, '0000000109', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392305', 1, 1, 1, 1, 128),
(110, '0000000110', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392345', 1, 1, 1, 1, 129),
(111, '0000000111', '', 0, '', '0.00', '1', '28000.00', '28000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599392379', 1, 1, 1, 1, 130),
(112, '0000000112', '', 0, '', '0.00', '5', '138000.00', '137995.00', '0.00', 0, 0, 1, 1, 0, '5.00', '1599395552', 1, 1, 1, 1, 131),
(113, '0000000113', '', 0, '', '0.00', '2', '75000.00', '75000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599395955', 1, 1, 1, 1, 132),
(114, '0000000114', '', 0, '', '0.00', '2', '75000.00', '78000.00', '3000.00', 0, 0, 1, 1, 0, '0.00', '1599750983', 1, 1, 1, 1, 133),
(115, '0000000115', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1599751279', 1, 1, 1, 1, 134),
(116, '0000000116', '', 0, '', '0.00', '2', '24000.00', '22000.00', '0.00', 0, 0, 1, 1, 0, '2000.00', '1600085723', 1, 1, 1, 1, 135),
(117, '0000000117', '', 0, '', '0.00', '2', '24000.00', '24000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1600085980', 1, 1, 1, 1, 136),
(118, '0000000118', '', 0, '', '0.00', '6', '280000.00', '280000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1600086109', 1, 1, 1, 1, 137),
(119, '0000000119', '', 0, '', '0.00', '8', '48000.00', '48000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1600086164', 1, 1, 1, 1, 138),
(120, '0000000120', '', 0, '', '0.00', '17', '1135000.00', '1135000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1600086297', 1, 1, 1, 1, 139),
(121, '0000000121', '', 0, '', '0.00', '1', '40000.00', '40000.00', '0.00', 0, 0, 1, 1, 0, '0.00', '1600095638', 1, 2, 1, 2, 140),
(122, '0000000122', '', 0, '', '0.00', '1', '40000.00', '39000.00', '0.00', 0, 0, 1, 1, 0, '1000.00', '1600336624', 1, 2, 1, 2, 141);

-- --------------------------------------------------------

--
-- Table structure for table `sale_list_header_bak`
--

CREATE TABLE `sale_list_header_bak` (
  `ID_auto` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address_all` text NOT NULL,
  `sumsale_discount` decimal(65,2) NOT NULL,
  `sumsale_num` varchar(255) NOT NULL,
  `sumsale_price` decimal(65,2) NOT NULL,
  `money_from_customer` decimal(65,2) NOT NULL,
  `money_changeto_customer` decimal(65,2) NOT NULL,
  `vat` int(2) NOT NULL,
  `product_score_all` int(11) NOT NULL,
  `sale_type` int(1) NOT NULL COMMENT '1=ขายปลีก,2=ขายส่ง',
  `pay_type` int(1) NOT NULL COMMENT '1=เงินสด,2=โอน,3=บัตรเครดิต,4=ค้างชำระ',
  `reserv` int(1) NOT NULL COMMENT '1=จอง',
  `discount_last` decimal(65,2) NOT NULL COMMENT 'ส่วนลดท้ายบิล',
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `number_for_cus` int(11) NOT NULL,
  `user_name_del` varchar(255) NOT NULL,
  `del_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `shift_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `shift_start_time` varchar(255) NOT NULL,
  `shift_end_time` varchar(255) NOT NULL,
  `shift_money_start` decimal(65,2) NOT NULL,
  `shift_money_end` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`shift_id`, `user_id`, `user_name`, `shift_start_time`, `shift_end_time`, `shift_money_start`, `shift_money_end`) VALUES
(1, 1, 'Admin', '1578329001', '', '100000.00', '0.00'),
(2, 2, 'sale', '1600095215', '', '100000.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `store_manager`
--

CREATE TABLE `store_manager` (
  `store_id` int(11) NOT NULL,
  `store_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `store_storename` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'ชื่อร้าน',
  `store_tel` varchar(255) NOT NULL,
  `store_email` varchar(255) NOT NULL,
  `store_password` varchar(255) NOT NULL,
  `max_user` int(10) NOT NULL,
  `store_type` int(1) NOT NULL COMMENT '0=pos , 1=ร้านอาหาร'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_manager`
--

INSERT INTO `store_manager` (`store_id`, `store_name`, `store_storename`, `store_tel`, `store_email`, `store_password`, `max_user`, `store_type`) VALUES
(1, 'yo', 'pos', '0987654321', 'pos@cus2mer.co', '0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `supplier_card_tax` varchar(255) NOT NULL,
  `supplier_code` varchar(255) NOT NULL,
  `supplier_bd` varchar(255) NOT NULL,
  `supplier_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `supplier_tel` varchar(255) NOT NULL,
  `supplier_image` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `supplier_name`, `supplier_card_tax`, `supplier_code`, `supplier_bd`, `supplier_address`, `supplier_tel`, `supplier_image`, `owner_id`, `user_id`, `store_id`, `add_date`) VALUES
(1, 'test', '001', '02', '10-09-1986', 'ttt', '0008888', '', 1, 1, 1, '2020-09-06 08:41:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_owner`
--

CREATE TABLE `user_owner` (
  `user_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_type` int(1) NOT NULL COMMENT '0=พนักงานเปิดบิล,1=พนักงานขาย,2=สต๊อกร้าน ,3=พนักงานคุมคลังสินค้าใหญ่,4=admin ร้าน',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `store_type` int(1) NOT NULL COMMENT '0=pos , 1=ร้านอาหาร',
  `food_brand_id` int(11) NOT NULL,
  `apartment_brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_owner`
--

INSERT INTO `user_owner` (`user_id`, `owner_id`, `store_id`, `supplier_id`, `name`, `user_email`, `user_password`, `user_type`, `create_date`, `store_type`, `food_brand_id`, `apartment_brand_id`) VALUES
(1, 1, 1, 0, 'Admin', 'admin', '5594b3d057f8dac26dcc9ccef2c06d95', 4, '2018-03-07 10:48:16', 0, 0, 0),
(2, 1, 1, 0, 'sale', 's1', '6aecf3c31b664261934b9e9c19f1e30f', 1, '2020-08-26 09:06:37', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL,
  `vendor_name` longtext CHARACTER SET utf8 NOT NULL,
  `vendor_address` longtext CHARACTER SET utf8 NOT NULL,
  `id_vat` varchar(255) NOT NULL,
  `vat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `vendor_address`, `id_vat`, `vat`) VALUES
(1, 'k', 'k', '01', 0),
(2, 'j', 'j', '001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `whbig_exportproduct_datail`
--

CREATE TABLE `whbig_exportproduct_datail` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `zone_name` varchar(255) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_price` int(255) NOT NULL,
  `product_sale_num` int(255) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_score` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `whbig_exportproduct_header`
--

CREATE TABLE `whbig_exportproduct_header` (
  `ID` int(11) NOT NULL,
  `sale_runno` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address_all` text NOT NULL,
  `sumsale_discount` decimal(65,2) NOT NULL,
  `sumsale_num` int(255) NOT NULL,
  `sumsale_price` decimal(65,2) NOT NULL,
  `money_from_customer` decimal(65,2) NOT NULL,
  `money_changeto_customer` decimal(65,2) NOT NULL,
  `vat` int(2) NOT NULL,
  `product_score_all` int(11) NOT NULL,
  `adddate` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `whbig_importproduct_detail`
--

CREATE TABLE `whbig_importproduct_detail` (
  `importproduct_detail_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `importproduct_detail_pricebase` decimal(65,2) NOT NULL,
  `importproduct_detail_num` int(11) NOT NULL,
  `importproduct_detail_date` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `whbig_importproduct_header`
--

CREATE TABLE `whbig_importproduct_header` (
  `importproduct_header_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `importproduct_header_refcode` varchar(50) NOT NULL,
  `importproduct_header_num` int(11) NOT NULL,
  `importproduct_header_amount` decimal(65,2) NOT NULL,
  `importproduct_header_remark` varchar(255) NOT NULL,
  `importproduct_header_date` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wh_exportproduct_detail`
--

CREATE TABLE `wh_exportproduct_detail` (
  `importproduct_detail_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `importproduct_detail_pricebase` decimal(65,2) NOT NULL,
  `importproduct_detail_num` int(11) NOT NULL,
  `importproduct_detail_date` varchar(255) NOT NULL,
  `lotno` varchar(255) NOT NULL,
  `date_end` varchar(255) NOT NULL,
  `date_end2` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wh_exportproduct_header`
--

CREATE TABLE `wh_exportproduct_header` (
  `importproduct_header_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `importproduct_header_refcode` varchar(50) NOT NULL,
  `importproduct_header_num` int(11) NOT NULL,
  `importproduct_header_amount` decimal(65,2) NOT NULL,
  `importproduct_header_remark` varchar(255) NOT NULL,
  `importproduct_header_date` varchar(255) NOT NULL,
  `lotno` varchar(255) NOT NULL,
  `date_end` varchar(255) NOT NULL,
  `date_end2` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wh_importproduct_detail`
--

CREATE TABLE `wh_importproduct_detail` (
  `importproduct_detail_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `importproduct_detail_pricebase` decimal(65,2) NOT NULL,
  `importproduct_detail_num` int(11) NOT NULL,
  `importproduct_detail_date` varchar(255) NOT NULL,
  `lotno` varchar(255) NOT NULL,
  `date_end` varchar(255) NOT NULL,
  `date_end2` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wh_importproduct_detail`
--

INSERT INTO `wh_importproduct_detail` (`importproduct_detail_id`, `importproduct_header_code`, `product_id`, `importproduct_detail_pricebase`, `importproduct_detail_num`, `importproduct_detail_date`, `lotno`, `date_end`, `date_end2`, `owner_id`, `user_id`, `store_id`, `status`) VALUES
(1, 1575462767, 1, '7000.00', 10, '1575462767', '1', '04-12-2019', '1575392400', 1, 1, 1, 0),
(2, 1575463202, 1, '7000.00', 1, '1575463202', '', '', '0', 1, 1, 1, 0),
(3, 1575465487, 1, '7000.00', 1, '1575465487', '', '', '0', 1, 1, 1, 0),
(4, 1576153770, 2, '0.00', 50, '1576153770', '', '', '0', 1, 1, 1, 0),
(5, 1576154352, 4, '0.00', 10, '1576154352', '', '', '0', 1, 1, 1, 0),
(6, 1576154539, 5, '0.00', 50, '1576154539', '', '', '0', 1, 1, 1, 0),
(7, 1576154698, 5, '0.00', 10, '1576154698', '', '', '0', 1, 1, 1, 0),
(8, 1594515759, 281, '0.00', 18, '1594515759', '1', '', '0', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wh_importproduct_header`
--

CREATE TABLE `wh_importproduct_header` (
  `importproduct_header_id` int(11) NOT NULL,
  `importproduct_header_code` int(11) NOT NULL,
  `importproduct_header_refcode` varchar(50) NOT NULL,
  `importproduct_header_num` int(11) NOT NULL,
  `importproduct_header_amount` decimal(65,2) NOT NULL,
  `importproduct_header_remark` varchar(255) NOT NULL,
  `importproduct_header_date` varchar(255) NOT NULL,
  `lotno` varchar(255) NOT NULL,
  `date_end` varchar(255) NOT NULL,
  `date_end2` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wh_importproduct_header`
--

INSERT INTO `wh_importproduct_header` (`importproduct_header_id`, `importproduct_header_code`, `importproduct_header_refcode`, `importproduct_header_num`, `importproduct_header_amount`, `importproduct_header_remark`, `importproduct_header_date`, `lotno`, `date_end`, `date_end2`, `owner_id`, `user_id`, `store_id`) VALUES
(1, 1575462767, '', 10, '70000.00', '', '1575462767', '1', '04-12-2019', '1575392400', 1, 1, 1),
(2, 1575463202, '', 1, '7000.00', '', '1575463202', '', '', '0', 1, 1, 1),
(3, 1575465487, '', 1, '7000.00', '', '1575465487', '', '', '0', 1, 1, 1),
(4, 1576153770, '', 50, '0.00', '', '1576153770', '', '', '0', 1, 1, 1),
(5, 1576154352, '', 10, '0.00', '', '1576154352', '', '', '0', 1, 1, 1),
(6, 1576154539, '', 50, '0.00', '', '1576154539', '', '', '0', 1, 1, 1),
(7, 1576154698, '', 10, '0.00', '', '1576154698', '', '', '0', 1, 1, 1),
(8, 1594515759, '', 18, '0.00', '', '1594515759', '1', '', '0', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wh_product_category`
--

CREATE TABLE `wh_product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wh_product_category`
--

INSERT INTO `wh_product_category` (`product_category_id`, `product_category_name`, `owner_id`, `user_id`, `store_id`) VALUES
(1, 'ເຄື່ອງຄົວ', 1, 1, 1),
(2, 'ເຄື່ອງໃຊ້', 1, 1, 1),
(3, 'gg', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wh_product_list`
--

CREATE TABLE `wh_product_list` (
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_date_end` varchar(255) NOT NULL,
  `product_date_end2` varchar(255) NOT NULL,
  `product_des` longtext NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_price` decimal(65,2) NOT NULL,
  `product_wholesale_price` decimal(65,2) NOT NULL,
  `product_pricebase` decimal(65,2) NOT NULL,
  `product_price_discount` decimal(65,2) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `product_unit_id` int(11) NOT NULL,
  `product_stock_num` varchar(255) NOT NULL,
  `product_num_all` varchar(255) NOT NULL,
  `product_whbig_num` int(11) NOT NULL,
  `product_price_value` decimal(65,2) NOT NULL,
  `product_score` decimal(65,2) NOT NULL,
  `product_rank` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `product_num_min` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wh_product_list`
--

INSERT INTO `wh_product_list` (`product_id`, `product_code`, `product_name`, `product_date_end`, `product_date_end2`, `product_des`, `product_image`, `product_price`, `product_wholesale_price`, `product_pricebase`, `product_price_discount`, `product_category_id`, `product_unit_id`, `product_stock_num`, `product_num_all`, `product_whbig_num`, `product_price_value`, `product_score`, `product_rank`, `zone_id`, `product_num_min`, `supplier_id`, `owner_id`, `user_id`, `store_id`, `add_date`) VALUES
(1, '1', 'ມີດດ້າມໄມ້ປາຍແຫຼມ', '', '0', '', '', '14000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(2, '2', 'ມີດດ້າມໄມ້ປາຍປູ້', '', '', '', '', '14000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(3, '3', 'ມີດດ້າມດຳປາຍແຫຼມ', '', '', '', '', '16000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(4, '4', 'ມີດດ້າມດຳນ້ອຍ', '', '', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(5, '5', 'ມີດສອງຄົມດ້າມເຫຼືອງ', '', '', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '-3', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(6, '6', 'ຂຽງໄມ້ສີ່ຫຼ່ຽມລຸ້ນໜາໃຫຍ່', '', '', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '-10', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(7, '7', 'ຂຽງໄມ້ຫຼ່ຽມແປລຸ້ນບາງໃຫຍ່', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(8, '8', 'ຂຽງໄມ້ສີ່ຫຼ່ຽມນ້ອຍບາງ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(9, '9', 'ເຂັ້ງຢາງໃຫຍ່ສຸດ', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(10, '10', 'ເຂັ້ງຢາງກາງ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(11, '11', 'ເຂັ້ງຢາງນ້ອຍ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(12, '12', 'ຊາມມີຫູລຸ້ນໜານ້ອຍ', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(13, '13', 'ຊາມດຳມີຫູເບີ8ກາງ', '', '0', '', '', '48000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(14, '14', 'ຊາມດຳມີຫູເບີ10ກາງ', '', '0', '', '', '55000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(15, '15', 'ຊາມດຳມີຫູເບີ12ກາງ', '', '0', '', '', '68000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(16, '16', 'ຊາມກາເສືອດຳເບີ59', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(17, '17', 'ຖ້ວຍດິນນ້ອຍ', '', '0', '', '', '2000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(18, '18', 'ຖ້ວຍດິນກາງ', '', '0', '', '', '4000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(19, '19', 'ຖ້ວຍດິນໃຫຍ່', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(20, '20', 'ຖ້ວຍດິນໜາມີຄໍນ້ອຍ', '', '0', '', '', '8000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(21, '21', 'ຖ້ວຍດິນໜາມີຄໍໃຫຍ່', '', '0', '', '', '10000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(22, '22', 'ຖ້ວຍດິນໜາມີຄໍໃຫຍ່ຈຳໂບ້', '', '0', '', '', '11000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(23, '23', 'ຈານດິນນ້ອຍ', '', '0', '', '', '2000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(24, '24', 'ຈານດິນກາງ', '', '0', '', '', '3000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(25, '25', 'ຈານດິນໃຫຍ່', '', '0', '', '', '4000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(26, '26', 'ຈານດິນໃຫຍ່ຈຳໂບ້', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:36:59'),
(27, '27', 'ຮ້ານພະເບີ13', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(28, '28', 'ຮ້ານພະເບີ14', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(29, '29', 'ຮ້ານພະເບີ16', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(30, '30', 'ຮ້ານພະເບີ18', '', '0', '', '', '80000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(31, '31', 'ຮ້ານພະເບີ20', '', '0', '', '', '110000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(32, '32', 'ຈວຍຮ່າຍເຝີເລດນ້ອຍ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(33, '33', 'ກະຕ່າໃສ່ເຄື່ອງໃຫຍ່', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(34, '34', 'ຕັກນ້ຳກ້ອນເລດເບີ0', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(35, '35', 'ຕັກນ້ຳກ້ອນເລດເບີ1', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(36, '36', 'ຕັກນ້ຳກ້ອນເລດເບີ2', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(37, '37', 'ຊັ້ນໃສ່ວາງເຄື່ອງ4 ຊັ້ນລຸ້ນໜາຕາຖີ່', '', '0', '', '', '120000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(38, '38', 'ຊັ້ນໃສ່ວາງເຄື່ອງ4 ຊັ້ນລຸ້ນບາງຕາຖີ່', '', '0', '', '', '105000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(39, '39', 'ຊັ້ນໃສ່ວາງເຄື່ອງ3 ຊັ້ນລຸ້ນໜາຕາຖີ່', '', '0', '', '', '100000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(40, '40', 'ຊັ້ນໃສ່ວາງເຄື່ອງ3 ຊັ້ນລຸ້ນບາງຕາຖີ່', '', '0', '', '', '85000.00', '0.00', '0.00', '0.00', 1, 0, '-2', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(41, '41', '3ຊັ້ນລຸ້ນຕ່ຳເຫຼັກໃສ່ຈານ', '', '0', '', '', '42000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(42, '42', 'ຊິງ5kg', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(43, '43', 'ຊິງ15kg', '', '0', '', '', '135000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(44, '44', 'ຊິງ30kg', '', '0', '', '', '145000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(45, '45', 'ເຕົາລີດottoແຫ້ງທຳມະດາ', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:00'),
(46, '46', 'ເຕົາລີດmaxtonແຫ້ງທຳມະດາ', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(47, '47', 'ເຕົາລີດlovestarແຫ້ງທຳມະດາ', '', '0', '', '', '80000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(48, '48', 'ເຕົາລີດMITSHU ອາຍນ້ຳ', '', '0', '', '', '105000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(49, '49', 'ເຕົາລີດ Faber ທຳມະດາ', '', '0', '', '', '90000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(50, '50', 'ໝໍ້ຫຸງເຂົ້າ Sunny ນ້ອຍ (ໜ້າຂາວ)', '', '0', '', '', '95000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(51, '51', 'ໝໍ້ຫຸງເຂົ້າ Sunny ນ້ອຍ (ໜ້າດຳ)', '', '0', '', '', '105000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(52, '52', 'ໝໍ້ຫຸງເຂົ້າ Sunny 1.8 Lໃຫຍ່(ໜ້າຂາວ)', '', '0', '', '', '120000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(53, '53', 'ໝໍ້ຫຸງເຂົ້າ Sunny 1.8 Lໃຫຍ່(ໜ້າດຳ)', '', '0', '', '', '130000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(54, '54', 'ໝໍ້ຫຸງເຂົ້າ Inter ໃຫຍ່', '', '0', '', '', '195000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(55, '55', 'ໝໍ້ຫຸງເຂົ້າມີຊຶງນ້ອຍ Otto', '', '0', '', '', '115000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(56, '56', 'ໝໍ້ຫຸງເຂົ້າມີຊຶງໃຫຍ່ Otto', '', '0', '', '', '130000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(57, '57', 'ໝໍ້ແດງໃຫຍ່ໃນດຳ2 ຊັ້ນ (ມີຊຶງ)', '', '0', '', '', '260000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(58, '58', 'ໝໍ້ແດງໃຫຍ່ໃນດຳ (ບໍ່ມີຊຶງ)', '', '0', '', '', '230000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(59, '59', 'ໝໍ້ແດງກາງໃນດຳ (ບໍ່ມີຊຶງ)', '', '0', '', '', '150000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(60, '60', 'ໝໍ້ແດງກາງໃນດຳ (ມີຊຶງ)', '', '0', '', '', '150000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(61, '61', 'ໝໍ້ແດງກາງໃນຂາວ (ມີຊຶງ)', '', '0', '', '', '165000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(62, '62', 'ເຄື່ອງບົດຊີ້ນນ້ອຍ', '', '0', '', '', '90000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(63, '63', 'ເຄື່ອງບົດຊີ້ນໃຫຍ່ເປັນຊຸດ', '', '0', '', '', '150000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(64, '64', 'ເຄື່ອງປັ່ນໝາກໄມ້ otto (ຢາງ)', '', '0', '', '', '130000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(65, '65', 'ເຄື່ອງປັ່ນໝາກໄມ້ Jiplai', '', '0', '', '', '185000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(66, '66', 'ເຄື່ອງປັ່ນ Otto (ແກ້ວ)', '', '0', '', '', '180000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(67, '67', 'ເຄື່ອງປັ່ນສະແຕນເຫຼດ (ໂຖແກ້ວ)', '', '0', '', '', '230000.00', '0.00', '0.00', '0.00', 1, 0, '-2', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(68, '68', 'ໝໍ້ໜຶ້ງນ້ອຍ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(69, '69', 'ໝໍ້ໜຶ້ງໃຫຍ່', '', '0', '', '', '28000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(70, '70', 'ອັນປົກແມງວັນໃຫຍ່', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(71, '71', 'ອັນປົກແມງວັນກາງ', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(72, '72', 'ກ່ອງໃສ່ຖ້ວຍນ້ອຍຂອງໄທ', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '-2', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(73, '73', 'ກ່ອງໃສ່ຖ້ວຍກາງ', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(74, '74', 'ກ່ອງໃສ່ຖ້ວຍໃຫຍ່', '', '0', '', '', '85000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:01'),
(75, '75', 'ກ່ອງໃສ່ຖ້ວຍໃຫຍ່ຈຳໂບ້', '', '0', '', '', '95000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(76, '76', 'ກ່ອງໃສ່ຈອກໃຫຍ່', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '-2', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(77, '77', 'ກ່ອງໃສ່ຈອກໃຫຍ່ແປຍາວ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(78, '78', 'ກ່ອງໃສ່ເຄື່ອງຝາລ໋ອກມີລໍ້ໃຫຍ່', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(79, '79', 'ກ່ອງໃສ່ເຄື່ອງຝາລ໋ອກມີລໍ້ກາງ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 1, 0, '-2', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(80, '80', 'ກ່ອງໃສ່ເຄື່ອງນ້ອຍລຸ້ນໃສ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(81, '81', 'ໝໍ້ດາດນ້ອຍລຸ້ນໜາ', '', '0', '', '', '36000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(82, '82', 'ໝໍ້ດາດກາງລຸ້ນໜາ', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(83, '83', 'ໝໍ້ດາດໃຫຍ່ລຸ້ນໜາ', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(84, '84', 'ໝໍ້ກາແຂ້ເບີ32(ໝໍ້ແກງ)', '', '0', '', '', '90000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(85, '85', 'ໝໍ້ກາແຂ້(ໝໍ້ແກງ)ເບີ 30', '', '0', '', '', '85000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(86, '86', 'ໝໍ້ກາແຂ້(ໝໍ້ແກງ)ເບີ 28', '', '0', '', '', '80000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(87, '87', 'ໝໍ້ກາແຂ້(ໝໍ້ແກງ)ເບີ 26', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(88, '88', 'ໝໍ້ກາແຂ້(ໝໍ້ແກງ)ເບີ 24', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(89, '89', 'ໝໍ້ກາແຂ້(ໝໍ້ແກງ)ເບີ 22', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(90, '90', 'ໝໍ້ແກງທຳມະດາເບີ 16', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(91, '91', 'ໝໍ້ແກງທຳມະດາເບີ 18', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(92, '92', 'ໝໍ້ແກງທຳມະດາເບີ 20', '', '0', '', '', '23000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(93, '93', 'ໝໍ້ແກງທຳມະດາເບີ 22', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(94, '94', 'ໝໍ້ແກງທຳມະດາເບີ 24', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(95, '95', 'ໝໍ້ແກງທຳມະດາເບີ 26', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(96, '96', 'ໝໍ້ແກງທຳມະດາເບີ 28', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(97, '97', 'ໝໍ້ແກງທຳມະດາເບີ 30', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(98, '98', 'ຕູ້4ຊັ້ນລາຍກາຕູນ', '', '0', '', '', '235000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(99, '99', 'ຕູ້3ຊັ້ນລາຍກາຕູນ', '', '0', '', '', '215000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(100, '100', 'ຕູ້3ຊັ້ນສີລ້ວນ', '', '0', '', '', '150000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(101, '101', 'ຕູ້4ຊັ້ນສີລ້ວນ', '', '0', '', '', '180000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(102, '102', 'ຕູ້5ຊັ້ນສີລ້ວນ', '', '0', '', '', '210000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(103, '103', 'ພາເຂົ້າໂລໃຫຍ່', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(104, '104', 'ພາເຂົ້າໂລກາງ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(105, '105', 'ພາເຂົ້າເຫຼດໃຫຍ່', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(106, '106', 'ພາເຂົ້າເຫຼດກາງ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(107, '107', 'ພາເຂົ້າເຫຼດນ້ອຍ', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(108, '108', 'ສາດຢາງພາລາ5m', '', '0', '', '', '115000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(109, '109', 'ສາດຢາງພາລາ2m', '', '0', '', '', '38000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(110, '110', 'ຊຸດຖັງຖູພື້ນລຸ້ນຢຽບ', '', '0', '', '', '150000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(111, '111', 'ຊຸດຖັງຖູພື້ນລຸ້ນໃຫຍ່', '', '0', '', '', '95000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(112, '112', 'ຊຸດຖັງຖູພື້ນລຸ້ນນ້ອຍ', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:02'),
(113, '113', 'ຜ້າເຊັດຕີນລາວພື້ນນ້ອຍ', '', '0', '', '', '3000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:03'),
(114, '114', 'ຜ້າເຊັດຕີນລາວພື້ນກາງ', '', '0', '', '', '4000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:03'),
(115, '115', 'ຜ້າເຊັດຕີນລາວມົນ', '', '0', '', '', '5000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:03'),
(116, '116', 'ຜ້າເຊັດຕີນລາວມົນໄຂ່', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:37:03'),
(147, '117', 'ຜ້າເຊັດຕີນຂົນນຸ້ມນ້ອຍ', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:43:02'),
(148, '118', 'ຜ້າເຊັດຕີນຂົນນຸ້ມໃຫຍ່', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:43:51'),
(149, '1576406815', 'ເຫຼັກປີ້ງເຫຼດນ້ອຍ', '', '0', '', '', '13000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:46:55'),
(150, '1576406868', 'ເຫຼັກປີ້ງເຫຼດກາງ', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:47:48'),
(151, '1576406909', 'ເຫຼັກປີ້ງເຫຼດໃຫຍ່', '', '0', '', '', '22000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:48:29'),
(152, '1576406968', 'ເຫຼັກປີ້ງໂລນ້ອຍ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:49:28'),
(153, '1576407005', 'ເຫຼັກປີ້ງໂລກາງ', '', '0', '', '', '8000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:50:05'),
(154, '1576407032', 'ເຫຼັກປີ້ງໂລຂະໜາດ 40 cm', '', '0', '', '', '10000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:50:32'),
(155, '1576407068', 'ເຫຼັກປີ້ງໂລຂະໜາດ 50 cm', '', '0', '', '', '12000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:51:08'),
(156, '1576407088', 'ເຫຼັກປີ້ງໂລຂະໜາດ 60 cm', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:51:28'),
(157, '1576407109', 'ເຫຼັກປີ້ງໂລຂະໜາດ 70 cm', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:51:49'),
(158, '1576407177', 'ຂຽງໄມ້ລາວນ້ອຍ', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:52:57'),
(159, '1576407205', 'ຂຽງໄມ້ລາວກາງເບີ2', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:53:25'),
(160, '1576407239', 'ຂຽງໄມ້ລາວໃຫຍ່ ເບີ 3', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:53:59'),
(161, '1576407280', 'ຂຽງໄມ້ລາວໃຫຍ່ຈຳໂບ້ ເບີ 4 ລຸ້ນໜາ', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:54:40'),
(162, '1576407319', 'ດອກໄຟຍາວເປັນຊຸດ 40 w', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:55:19'),
(163, '1576407364', 'ດອກໄຟສັ້ນ 20 w', '', '0', '', '', '18000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 10:56:04'),
(164, '1576407409', 'ດອກໄຟຫົວກຽວໃຫຍ່ 50 w', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:56:49'),
(165, '1576407426', 'ດອກໄຟຫົວກຽວ 105 w', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:57:06'),
(166, '1576407483', 'ໝໍ້ຂາງກາແຂ້ເບີ 14', '', '0', '', '', '47000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:58:03'),
(167, '1576407498', 'ໝໍ້ຂາງກາແຂ້ເບີ 15', '', '0', '', '', '55000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:58:18'),
(168, '1576407511', 'ໝໍ້ຂາງກາແຂ້ເບີ 16', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:58:31'),
(169, '1576407526', 'ໝໍ້ຂາງກາແຂ້ເບີ 17', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:58:46'),
(170, '1576407542', 'ໝໍ້ຂາງກາແຂ້ເບີ 18', '', '0', '', '', '75000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:59:02'),
(171, '1576407585', 'ຈ່ອມ ເບີ M', '', '0', '', '', '48000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:59:45'),
(172, '1576407598', 'ຈ່ອມ ເບີ L', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 10:59:58'),
(173, '1576407628', 'ໂຕະຂາຍເລກ', '', '0', '', '', '115000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:00:28'),
(174, '1576407649', 'ໂຕະເຫຼັກກາງ', '', '0', '', '', '138000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:00:49'),
(175, '1576407725', 'ໂຕະເຫຼັກໃຫຍ່', '', '0', '', '', '158000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:02:05'),
(176, '1576407813', 'ໂຕະ ກຂຄ ເຫຼັກ', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:03:33'),
(177, '1576407864', 'ໂຕະ ກຂຄ ຢາງໃຫຍ່', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:04:24'),
(178, '1576407894', 'ໂຕະ ກຂຄ ຢາງນ້ອຍ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:04:54'),
(179, '1576407928', 'ຊາມອາບນ້ຳແອນ້ອຍ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:05:28'),
(180, '1576407955', 'ຕຽງພັບໄທ', '', '0', '', '', '210000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:05:55'),
(181, '1576408061', 'ຕຽງພັບຫວຽດ', '', '0', '', '', '130000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:07:41'),
(182, '1576408092', 'ຮາວຕາກເຄື່ອງຍາວ', '', '0', '', '', '55000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:08:12'),
(183, '1576408133', 'ຮາວຕາກເຄື່ອງຈຳໂບ້', '', '0', '', '', '120000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:08:53'),
(184, '1576408174', 'ຮາວຕາກເຄື່ອງມີລໍ້ A8', '', '0', '', '', '140000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:09:34'),
(185, '1576408202', 'ໂຕະລີດເຄື່ອງໃຫຍ່', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:10:02'),
(186, '1576408228', 'ໂຕະລີດເຄື່ອງນ້ອຍ', '', '0', '', '', '55000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:10:28'),
(187, '1576408306', 'ທິດຊູຍາວແພັກ 4 ອັນ', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:11:46'),
(188, '1576408345', 'ກາວຕິດໜູແຖມຖາດຮອງ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:12:25'),
(189, '1576408370', 'ອາຫານໜູ', '', '0', '', '', '4000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:12:50'),
(190, '1576408400', 'ສໍຂີດມົດແຖມກັບໄຟ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '-4', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 11:13:20'),
(191, '1576408425', ' ຫີນຝົນມີດ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:13:45'),
(192, '1576408478', 'ກາວລາເຕັກປ໋ອງນ້ອຍ (100g)', '', '0', '', '', '5000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:14:38'),
(193, '1576408515', 'ກາວລາເຕັກປ໋ອງກາງ (210g)', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:15:15'),
(194, '1576408590', 'ສຳລີຜັນຫູ (1 ກ່ອງ)', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '-5', '', 0, '0.00', '0.00', 2147483647, 0, 0, 0, 1, 1, 1, '2019-12-15 11:16:30'),
(195, '1576408627', 'ສະກ໋ອດໃສກໍ້ໃຫຍ່', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '-2', '', 0, '0.00', '0.00', 1111111111, 0, 0, 0, 1, 1, 1, '2019-12-15 11:17:07'),
(196, '1576408652', 'ສະກ໋ອດໃສກໍ້ນ້ອຍ', '', '0', '', '', '3000.00', '0.00', '0.00', '0.00', 0, 0, '-3', '', 0, '0.00', '0.00', 111111111, 0, 0, 0, 1, 1, 1, '2019-12-15 11:17:32'),
(197, '1576408731', 'ເຫຼັກໜີບເຈ້ຍນ້ອຍ 1 ກັບ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:18:51'),
(198, '1576408766', 'ຈຸບແປ້ງທາເດັກ', '', '0', '', '', '6000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:19:26'),
(199, '1576408796', 'ເຕົ້ານົມໃຫຍ່', '', '0', '', '', '10000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:19:56'),
(200, '1576408838', 'ໝອນນ້ອຍມີຊົບພ້ອມນຸ້ມ', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:20:38'),
(201, '1576408862', 'ໂຖຕົ້ນໄມ້', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:21:02'),
(202, '1576408896', 'ກະຕ່າ', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:21:36'),
(203, '1576408922', 'ເຄື່ອງຫຼິ້ນ', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:22:02'),
(204, '1576408936', 'ກະປຸກ', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:22:16'),
(205, '1576408967', 'ເຄື່ອງ 2 ຢ່າງ 5000', '', '0', '', '', '5000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:22:47'),
(206, '1576408992', ' ເຄື່ອງ 3500', '', '0', '', '', '3500.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:23:12'),
(207, '1576409020', 'ກະປຸກ 5 ລິດ', '', '0', '', '', '10000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:23:40'),
(208, '1576409046', 'ກະປຸກ 7 ລິດ', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:24:06'),
(209, '1576409071', 'ກະປຸກ  11 ລິດ', '', '0', '', '', '17000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 11:24:31'),
(210, '1576412992', 'ຜ້າຫົ່ມ Toto ນ້ອຍ', '', '0', '', '', '65000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:29:52'),
(211, '1576413031', 'ຜ້າຫົ່ມ Toto ກາງ', '', '0', '', '', '75000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:30:31'),
(212, '1576413054', 'ຜ້າຫົ່ມ Toto ໃຫຍ່', '', '0', '', '', '90000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:30:54'),
(213, '1576413090', 'ຜ້າຫົ່ມເປັນຊຸດໃຫຍ່', '', '0', '', '', '125000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:31:30'),
(214, '1576413138', 'ຄັນຫົ່ມໃຫຍ່ສຸດ', '', '0', '', '', '170000.00', '0.00', '0.00', '0.00', 0, 0, '-4', '', 0, '0.00', '0.00', 11111111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:32:18'),
(215, '1576413168', 'ຄັນຫົ່ມໃຫຍ່ຫຼາຍສີ', '', '0', '', '', '115000.00', '0.00', '0.00', '0.00', 0, 0, '-4', '', 0, '0.00', '0.00', 1111111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:32:48'),
(216, '1576413229', 'ເຕົາໄຟດິນເບີ (000)', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:33:49'),
(217, '1576413263', 'ເຕົາໄຟດິນເບີ (00)', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:34:23'),
(218, '1576413287', 'ເຕົາໄຟດິນເບີ (1)', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:34:47'),
(219, '1576413317', 'ເຕົາໄຟດິນເບີ (3)', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:35:17'),
(220, '1576413343', 'ເຕົາໄຟດິນເບີ (4)', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:35:43'),
(221, '1576413399', 'ເຕົາຊີມັງມີຊານ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:36:39'),
(222, '1576413426', 'ເຕົາຊີມັງນ້ອຍ', '', '0', '', '', '15000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:37:06'),
(223, '1576413449', 'ເຕົາຊີມັງໃຫຍ່', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:37:29'),
(224, '1576413515', 'ຄົກໃຫຍ່ສຸດ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 0, 0, '-21', '', 0, '0.00', '0.00', 111111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:38:36'),
(225, '1576413532', 'ຄົກໃຫຍ່', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:38:52'),
(226, '1576413568', 'ຄົກກາງ', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:39:28'),
(227, '1576413598', 'ຄົກກາງກະເທີນ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:39:58'),
(228, '1576413640', 'ຄົກກາງໃກ້ນ້ອຍ', '', '0', '', '', '18000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:40:40'),
(229, '1576413682', 'ໝໍ້ໂລເບີ 16 L', '', '0', '', '', '85000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:41:22'),
(230, '1576413737', 'ໝໍ້ໂລເບີ 20 L', '', '0', '', '', '95000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:42:17'),
(231, '1576413758', 'ໝໍ້ໂລເບີ 25 L', '', '0', '', '', '110000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:42:38'),
(232, '1576413781', 'ໝໍ້ໂລເບີ 30 L', '', '0', '', '', '125000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:43:01'),
(233, '1576413818', 'ໝໍ້ໂລເບີ 36 L', '', '0', '', '', '140000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:43:38'),
(234, '1576413839', 'ໝໍ້ໂລເບີ 50 L', '', '0', '', '', '180000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:43:59'),
(235, '1576413863', 'ໝໍ້ໂລເບີ 80 L', '', '0', '', '', '250000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:44:23'),
(236, '1576413881', 'ໝໍ້ໂລເບີ 100 L', '', '0', '', '', '290000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:44:41'),
(237, '1576413941', '4 ຊັ້ນເຄື່ອງຢາງ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:45:41'),
(238, '1576413965', '4 ຊັ້ນດຳຢາງ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:46:05'),
(239, '1576414005', '3 ຫຼ່ຽມມຸມ 3ຊັ້ນ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:46:45'),
(240, '1576414017', '3 ຫຼ່ຽມມຸມ 4ຊັ້ນ', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:46:57'),
(241, '1576414043', '4 ຊັ້ນໃສ່ເກີບ', '', '0', '', '', '28000.00', '0.00', '0.00', '0.00', 0, 0, '-24', '', 0, '0.00', '0.00', 11111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:47:23'),
(242, '1576414059', '5 ຊັ້ນໃສ່ເກີບ', '', '0', '', '', '28000.00', '0.00', '0.00', '0.00', 0, 0, '-28', '', 0, '0.00', '0.00', 1111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:47:39'),
(243, '1576414094', 'ຖົງລາຍແຊກນ້ອຍສຸດ', '', '0', '', '', '14000.00', '0.00', '0.00', '0.00', 0, 0, '-47', '', 0, '0.00', '0.00', 111, 0, 0, 0, 1, 1, 1, '2019-12-15 12:48:14'),
(244, '1576414128', 'ຖົງລາຍແຊກນ້ອຍສຸດເບີ 3', '', '0', '', '', '16000.00', '0.00', '0.00', '0.00', 0, 0, '-26', '', 0, '0.00', '0.00', 11, 0, 0, 0, 1, 1, 1, '2019-12-15 12:48:48'),
(245, '1576414160', 'ຖົງລາຍແຊກເບີ2', '', '0', '', '', '18000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:49:20'),
(246, '1576414180', 'ຖົງລາຍແຊກເບີ1', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:49:40'),
(247, '1576414201', 'ຖົງລາຍແຊກເບີ0', '', '0', '', '', '22000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:50:01'),
(248, '1576414236', 'ໄມ້ຂັດຫ້ອງນ້ຳມີຂາຕັ້ງ', '', '0', '', '', '12000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:50:36'),
(249, '1576414261', 'ໄມ້ຖູພື້ນຫ້ອງນ້ຳຍາວ', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:51:01'),
(250, '1576414282', 'ໄມ້ຖູພື້ນຫ້ອງຊົງຍາວທຳມະດາ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:51:22'),
(251, '1576414313', 'ໄມ້ຖູພື້ນບິດນ້ຳໄດ້', '', '0', '', '', '28000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:51:53'),
(252, '1576414340', 'ໄມ້ຖູພື້ນແບບຍອຍທຳມະດາ', '', '0', '', '', '22000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:52:20'),
(253, '1576414380', 'ໄມ້ຖູພື້ນເບີ 12', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:53:00'),
(254, '1576414394', 'ໄມ້ຖູພື້ນເບີ 15', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:53:14'),
(255, '1576414409', 'ໄມ້ຖູພື້ນເບີ 18', '', '0', '', '', '58000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:53:29'),
(256, '1576414537', 'ຟອຍຢາງກວດນ້ຳໂຕໃຫຍ່', '', '0', '', '', '22000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:55:37'),
(257, '1576414572', 'ອາໄຫຼ່ຜ້າເບີ 12', '', '0', '', '', '28000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:56:12'),
(258, '1576414611', 'ອາໄຫຼ່ຜ້າເບີ 15', '', '0', '', '', '33000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:56:51'),
(259, '1576414622', 'ອາໄຫຼ່ຜ້າເບີ 18', '', '0', '', '', '36000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:57:02'),
(260, '1576414720', 'ໄມ້ຖູພື້ນແບບມົນທຳມະດາ', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:58:40'),
(261, '1576414737', 'ໄມ້ຖູພື້ນແບບມົນເຫຼດ', '', '0', '', '', '45000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:58:57'),
(262, '1576414770', 'ພັດລົມຫວຽດນ້ອຍ', '', '0', '', '', '70000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:59:30'),
(263, '1576414790', 'ພັດລົມຫວຽດໃຫຍ່', '', '0', '', '', '80000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 12:59:50'),
(264, '1576414824', 'ພັດລົມ JIPLAI', '', '0', '', '', '200000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:00:24'),
(265, '1576414846', 'ຂາຕັ້ງຄົກນ້ອຍ', '', '0', '', '', '50000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:00:46'),
(266, '1576414862', 'ຂາຕັ້ງຄົກໃຫຍ່', '', '0', '', '', '60000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:01:02'),
(267, '1576414936', 'ໝໍ້ຕົ້ມນ້ຳຫົວເຈີ້ຍໂອໂຕ 2.5 L', '', '0', '', '', '125000.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:02:16'),
(268, '1576414968', 'ໝໍ້ຕົ້ມນ້ຳໂອໂຕ 3.8 L', '', '0', '', '', '260000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:02:48'),
(269, '1576414994', 'ໝໍ້ກາຕົ້ມນ້ຳນ້ອຍ', '', '0', '', '', '25000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:03:14'),
(270, '1576415011', 'ໝໍ້ກາຕົ້ມນ້ຳກາງ', '', '0', '', '', '30000.00', '0.00', '0.00', '0.00', 1, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:03:31'),
(271, '1576415026', 'ໝໍ້ກາຕົ້ມໃຫຍ່', '', '0', '', '', '35000.00', '0.00', '0.00', '0.00', 1, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:03:46'),
(272, '1576415054', 'ຟອຍກ້ານພ້າວ', '', '0', '', '', '5000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:04:14'),
(273, '1576415072', 'ຟອຍລາວ', '', '0', '', '', '7000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:04:32'),
(274, '1576415090', 'ຟອຍໄທດ້າມຢາງ', '', '0', '', '', '12000.00', '0.00', '0.00', '0.00', 2, 0, '98', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:04:50'),
(275, '1576415140', 'ກະຕ່າມີຝາອັດມີຫູຫິ້ວ', '', '0', '', '', '40000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:05:40'),
(276, '1576415176', 'ກະຕ່າມີຝາ 2 ດ້ານ ກາງ', '', '0', '', '', '20000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:06:16'),
(277, '1576415213', 'ກະຕ່າມີຝາ 2 ດ້ານ ນ້ອຍ', '', '0', '', '', '13000.00', '0.00', '0.00', '0.00', 2, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:06:53'),
(278, '1576415251', 'ກະຕ່າມີຝາ ດ້ານດຽວກາງ', '', '0', '', '', '10000.00', '0.00', '0.00', '0.00', 2, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:07:31'),
(279, '1576415279', 'ຢ່ວງຫ້ອຍເຄື່ອງມົນເຫຼດ', '', '0', '', '', '13000.00', '0.00', '0.00', '0.00', 2, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:07:59'),
(280, '1576415316', 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼດ', '', '0', '', '', '13000.00', '0.00', '0.00', '0.00', 2, 0, '-1', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2019-12-15 13:08:36'),
(281, '1576415348', 'ຢ່ວງຫ້ອຍເຄື່ອງເຫຼັກສີ', '', '0', '', 'pic/product_image/1/16000856371890450e30aa2c1779e3db6568c6b3a5.jpg', '12000.00', '0.00', '0.00', '0.00', 2, 0, '0', '18', 0, '0.00', '0.00', 1, 0, 5, 0, 1, 1, 1, '2019-12-15 13:09:08'),
(282, '1596725843', 'tt', '', '0', '', '', '0.00', '0.00', '0.00', '0.00', 0, 0, '', '', 0, '0.00', '0.00', 0, 0, 0, 0, 1, 1, 1, '2020-08-06 14:57:23');

-- --------------------------------------------------------

--
-- Table structure for table `wh_product_other`
--

CREATE TABLE `wh_product_other` (
  `pot_ID` int(11) NOT NULL,
  `product_ot_image` varchar(255) NOT NULL,
  `product_ot_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_ot_price` decimal(65,0) NOT NULL,
  `show_all` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wh_product_relation_list`
--

CREATE TABLE `wh_product_relation_list` (
  `prl_ID` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_id_relation` int(11) NOT NULL,
  `product_num_relation` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_name_relation` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_unit_relation` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wh_product_unit`
--

CREATE TABLE `wh_product_unit` (
  `product_unit_id` int(11) NOT NULL,
  `product_unit_name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(11) NOT NULL,
  `zone_name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `zone_name`, `owner_id`, `user_id`, `store_id`, `create_date`) VALUES
(1, 'A', 1, 1, 1, '2020-08-26 10:11:55'),
(2, 'B', 1, 1, 1, '2020-08-26 10:13:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barcode_ds`
--
ALTER TABLE `barcode_ds`
  ADD PRIMARY KEY (`ds_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `contact_from`
--
ALTER TABLE `contact_from`
  ADD PRIMARY KEY (`contact_from_id`);

--
-- Indexes for table `contact_grade`
--
ALTER TABLE `contact_grade`
  ADD PRIMARY KEY (`contact_grade_id`);

--
-- Indexes for table `contact_list`
--
ALTER TABLE `contact_list`
  ADD PRIMARY KEY (`contact_list_id`);

--
-- Indexes for table `customer_center`
--
ALTER TABLE `customer_center`
  ADD PRIMARY KEY (`cus_cen_id`),
  ADD UNIQUE KEY `cus_cen_email` (`cus_cen_email`),
  ADD UNIQUE KEY `cus_cen_tel` (`cus_cen_tel`);

--
-- Indexes for table `customer_group`
--
ALTER TABLE `customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `customer_level`
--
ALTER TABLE `customer_level`
  ADD PRIMARY KEY (`customer_level_id`);

--
-- Indexes for table `customer_owner`
--
ALTER TABLE `customer_owner`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `customer_reasonbuy`
--
ALTER TABLE `customer_reasonbuy`
  ADD PRIMARY KEY (`customer_reasonbuy_id`);

--
-- Indexes for table `customer_reasonnotbuy`
--
ALTER TABLE `customer_reasonnotbuy`
  ADD PRIMARY KEY (`customer_reasonnotbuy_id`);

--
-- Indexes for table `customer_sex`
--
ALTER TABLE `customer_sex`
  ADD PRIMARY KEY (`customer_sex_id`);

--
-- Indexes for table `money_rate`
--
ALTER TABLE `money_rate`
  ADD PRIMARY KEY (`curid`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`owner_id`),
  ADD UNIQUE KEY `owner_email` (`owner_email`);

--
-- Indexes for table `pawn`
--
ALTER TABLE `pawn`
  ADD PRIMARY KEY (`pawn_id`);

--
-- Indexes for table `pawnadddate`
--
ALTER TABLE `pawnadddate`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_price_cus`
--
ALTER TABLE `product_price_cus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_price_cus_group`
--
ALTER TABLE `product_price_cus_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_price_step`
--
ALTER TABLE `product_price_step`
  ADD PRIMARY KEY (`ps_ID`);

--
-- Indexes for table `product_return_datail`
--
ALTER TABLE `product_return_datail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_return_header`
--
ALTER TABLE `product_return_header`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_service`
--
ALTER TABLE `product_service`
  ADD PRIMARY KEY (`product_service_id`);

--
-- Indexes for table `purchase_buy_detail`
--
ALTER TABLE `purchase_buy_detail`
  ADD PRIMARY KEY (`importproduct_detail_id`);

--
-- Indexes for table `purchase_buy_header`
--
ALTER TABLE `purchase_buy_header`
  ADD PRIMARY KEY (`importproduct_header_id`);

--
-- Indexes for table `quotation_list_datail`
--
ALTER TABLE `quotation_list_datail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `quotation_list_header`
--
ALTER TABLE `quotation_list_header`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sale_list_cus2mer`
--
ALTER TABLE `sale_list_cus2mer`
  ADD PRIMARY KEY (`sc_ID`);

--
-- Indexes for table `sale_list_datail`
--
ALTER TABLE `sale_list_datail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sale_list_datail_bak`
--
ALTER TABLE `sale_list_datail_bak`
  ADD PRIMARY KEY (`ID_auto`);

--
-- Indexes for table `sale_list_header`
--
ALTER TABLE `sale_list_header`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `sale_runno` (`sale_runno`);

--
-- Indexes for table `sale_list_header_bak`
--
ALTER TABLE `sale_list_header_bak`
  ADD PRIMARY KEY (`ID_auto`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`shift_id`);

--
-- Indexes for table `store_manager`
--
ALTER TABLE `store_manager`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `user_owner`
--
ALTER TABLE `user_owner`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `whbig_exportproduct_datail`
--
ALTER TABLE `whbig_exportproduct_datail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `whbig_exportproduct_header`
--
ALTER TABLE `whbig_exportproduct_header`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `whbig_importproduct_detail`
--
ALTER TABLE `whbig_importproduct_detail`
  ADD PRIMARY KEY (`importproduct_detail_id`);

--
-- Indexes for table `whbig_importproduct_header`
--
ALTER TABLE `whbig_importproduct_header`
  ADD PRIMARY KEY (`importproduct_header_id`);

--
-- Indexes for table `wh_exportproduct_detail`
--
ALTER TABLE `wh_exportproduct_detail`
  ADD PRIMARY KEY (`importproduct_detail_id`);

--
-- Indexes for table `wh_exportproduct_header`
--
ALTER TABLE `wh_exportproduct_header`
  ADD PRIMARY KEY (`importproduct_header_id`);

--
-- Indexes for table `wh_importproduct_detail`
--
ALTER TABLE `wh_importproduct_detail`
  ADD PRIMARY KEY (`importproduct_detail_id`);

--
-- Indexes for table `wh_importproduct_header`
--
ALTER TABLE `wh_importproduct_header`
  ADD PRIMARY KEY (`importproduct_header_id`);

--
-- Indexes for table `wh_product_category`
--
ALTER TABLE `wh_product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `wh_product_list`
--
ALTER TABLE `wh_product_list`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `wh_product_other`
--
ALTER TABLE `wh_product_other`
  ADD PRIMARY KEY (`pot_ID`);

--
-- Indexes for table `wh_product_relation_list`
--
ALTER TABLE `wh_product_relation_list`
  ADD PRIMARY KEY (`prl_ID`);

--
-- Indexes for table `wh_product_unit`
--
ALTER TABLE `wh_product_unit`
  ADD PRIMARY KEY (`product_unit_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barcode_ds`
--
ALTER TABLE `barcode_ds`
  MODIFY `ds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_from`
--
ALTER TABLE `contact_from`
  MODIFY `contact_from_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_grade`
--
ALTER TABLE `contact_grade`
  MODIFY `contact_grade_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_list`
--
ALTER TABLE `contact_list`
  MODIFY `contact_list_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_center`
--
ALTER TABLE `customer_center`
  MODIFY `cus_cen_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_group`
--
ALTER TABLE `customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_level`
--
ALTER TABLE `customer_level`
  MODIFY `customer_level_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_owner`
--
ALTER TABLE `customer_owner`
  MODIFY `cus_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_reasonbuy`
--
ALTER TABLE `customer_reasonbuy`
  MODIFY `customer_reasonbuy_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_reasonnotbuy`
--
ALTER TABLE `customer_reasonnotbuy`
  MODIFY `customer_reasonnotbuy_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_sex`
--
ALTER TABLE `customer_sex`
  MODIFY `customer_sex_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `money_rate`
--
ALTER TABLE `money_rate`
  MODIFY `curid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `owner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pawn`
--
ALTER TABLE `pawn`
  MODIFY `pawn_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pawnadddate`
--
ALTER TABLE `pawnadddate`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_price_cus`
--
ALTER TABLE `product_price_cus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_price_cus_group`
--
ALTER TABLE `product_price_cus_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_price_step`
--
ALTER TABLE `product_price_step`
  MODIFY `ps_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_return_datail`
--
ALTER TABLE `product_return_datail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_return_header`
--
ALTER TABLE `product_return_header`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_service`
--
ALTER TABLE `product_service`
  MODIFY `product_service_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_buy_detail`
--
ALTER TABLE `purchase_buy_detail`
  MODIFY `importproduct_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchase_buy_header`
--
ALTER TABLE `purchase_buy_header`
  MODIFY `importproduct_header_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quotation_list_datail`
--
ALTER TABLE `quotation_list_datail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `quotation_list_header`
--
ALTER TABLE `quotation_list_header`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sale_list_cus2mer`
--
ALTER TABLE `sale_list_cus2mer`
  MODIFY `sc_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `sale_list_datail`
--
ALTER TABLE `sale_list_datail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT for table `sale_list_datail_bak`
--
ALTER TABLE `sale_list_datail_bak`
  MODIFY `ID_auto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sale_list_header`
--
ALTER TABLE `sale_list_header`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `sale_list_header_bak`
--
ALTER TABLE `sale_list_header_bak`
  MODIFY `ID_auto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `store_manager`
--
ALTER TABLE `store_manager`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_owner`
--
ALTER TABLE `user_owner`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `whbig_exportproduct_datail`
--
ALTER TABLE `whbig_exportproduct_datail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `whbig_exportproduct_header`
--
ALTER TABLE `whbig_exportproduct_header`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `whbig_importproduct_detail`
--
ALTER TABLE `whbig_importproduct_detail`
  MODIFY `importproduct_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `whbig_importproduct_header`
--
ALTER TABLE `whbig_importproduct_header`
  MODIFY `importproduct_header_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wh_exportproduct_detail`
--
ALTER TABLE `wh_exportproduct_detail`
  MODIFY `importproduct_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wh_exportproduct_header`
--
ALTER TABLE `wh_exportproduct_header`
  MODIFY `importproduct_header_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wh_importproduct_detail`
--
ALTER TABLE `wh_importproduct_detail`
  MODIFY `importproduct_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `wh_importproduct_header`
--
ALTER TABLE `wh_importproduct_header`
  MODIFY `importproduct_header_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `wh_product_category`
--
ALTER TABLE `wh_product_category`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wh_product_list`
--
ALTER TABLE `wh_product_list`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT for table `wh_product_other`
--
ALTER TABLE `wh_product_other`
  MODIFY `pot_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wh_product_relation_list`
--
ALTER TABLE `wh_product_relation_list`
  MODIFY `prl_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wh_product_unit`
--
ALTER TABLE `wh_product_unit`
  MODIFY `product_unit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
