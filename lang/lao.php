<?php

$data['lang_rank'] = 'ຄໍາສັ່ງ';
$data['lang_managername'] = 'ຊື່ຜູ້ຈັດການ';
$data['lang_manager'] = 'ຜູ້ຈັດການ';
$data['lang_tel'] = 'ໂທຫາ';
$data['lang_storename'] = 'ຊື່ຮ້ານ / ບໍລິສັດ';
$data['lang_email'] = 'ທີ່ຢູ່ອີເມວ';
$data['lang_usesystem'] = 'ໃຊ້ລະບົບ';
$data['lang_edit'] = 'ແກ້ໄຂ';
$data['lang_addmanager'] = 'ເພີ່ມຜູ້ຈັດການ';
$data['lang_search'] = 'ຄົ້ນຫາ';
$data['lang_searchlist'] = 'ຄົ້ນຫາ';
$data['lang_searchresult'] = 'ຜົນການຊອກຫາ';
$data['lang_downloadexcel'] = 'ດາວໂຫລດ Excel';

$data['lang_password'] = 'ລະຫັດຜ່ານ';
$data['lang_newpassword'] = 'ລະຫັດຜ່ານໃຫມ່';
$data['lang_changepassword'] = 'ປ່ຽນລະຫັດຜ່ານ';
$data['lang_passwordplz'] = 'ກະລຸນາໃສ່ລະຫັດຜ່ານຂອງທ່ານ.';
$data['lang_login'] = 'ເຂົ້າສູ່ລະບົບ';
$data['lang_logout'] = 'ອອກຈາກລະບົບ';
$data['lang_save'] = 'ບັນທຶກ';

$data['lang_possystem'] = 'POS Shop system';
$data['lang_foodsystem'] = 'FOOD ລະບົບຮ້ານອາຫານ';
$data['lang_apartmentsystem'] = 'APARTMENT ระบบที่พัก';

$data['lang_confirm'] = 'ຢືນຢັນ';
$data['lang_cannotusethisemail'] = 'ອີເມວນີ້ບໍ່ສາມາດໃຊ້ໄດ້.';
$data['lang_success'] = 'ສໍາເລັດ';
$data['lang_plz'] = 'ໂປດກອກຂໍ້ມູນທັງຫມົດ.';

$data['lang_cusreasonnotnuy'] = 'ເປັນຫຍັງຈຶ່ງບໍ່ສາມາດຊື້';
$data['lang_settingpriceforcus'] = 'ຕັ້ງຄ່າການຂາຍສິນຄ້າໃຫ້ກັບ';

$data['lang_staff'] = 'ພະນັກງານ';
$data['lang_brand'] = 'ສາຂາ';
$data['lang_brandname'] = 'ຊື່ສາຂາ';
$data['lang_addbrand'] = 'ເພີ່ມທະນາຄານ';
$data['lang_selectbrand'] = 'ເລືອກສາຂາ';
$data['lang_lookstock'] = 'View Inventory';
$data['lang_salereportstaff'] = 'ລາຍງານການຂາຍພະນັກງານ';
$data['lang_salereportbrand'] = 'ລາຍງານການຂາຍ';
$data['lang_reportreturnproduct'] = 'Returns Product';

$data['lang_addstaff'] = 'ເພີ່ມພະນັກງານ';
$data['lang_staffname'] = 'ຊື່ພະນັກງານ';
$data['lang_selectstaff'] = 'ເລືອກພະນັກງານ';
$data['lang_plzselectstaff'] = 'ກະລຸນາເລືອກພະນັກງານ.';

$data['lang_address'] = 'ທີ່ຢູ່';
$data['lang_tax'] = 'ຜູ້ຈ່າຍພາສີ';


$data['lang_selectbrandplz'] = 'ກະລຸນາເລືອກສາຂາ';
$data['lang_searchproductname'] = 'ຄົ້ນຫາໂດຍຊື່ຜະລິດຕະພັນ';
$data['lang_searchproductnameorscan'] = 'ລະຫັດ';
$data['lang_barcode'] = 'ລະຫັດ';
$data['lang_productname'] = 'ຊື່ຜະລິດຕະພັນ';

$data['lang_category'] = 'ປະເພດ';
$data['lang_categoryname'] = 'ຊື່ຫມວດຫມູ່';
$data['lang_selectcategory'] = 'ເລືອກຫມວດຫມູ່';

$data['lang_saleprice'] = 'ລາຄາຂາຍ';
$data['lang_discount'] = 'ລາຄາຜ່ອນຜັນ';
$data['lang_total'] = 'ຈໍານວນທີ່ຍັງເຫຼືອ';
$data['lang_estimatedrevenue'] = 'ລາຍໄດ້ປະມານ';

$data['lang_fromday'] = 'ຈາກວັນທີ';
$data['lang_today'] = 'ເຖິງວັນທີ';

$data['lang_saletotal'] = 'ຈໍານວນການຂາຍ';
$data['lang_cansale'] = 'ຂາຍ';
$data['lang_revenue'] = 'ໃບຮັບເງິນ';
$data['lang_cost'] = 'ຄ່າໃຊ້ຈ່າຍ';
$data['lang_profitlost'] = 'ກໍາໄຮ / ຂາດທຶນ';
$data['lang_roi'] = 'ROI';
$data['lang_price'] = 'ລາຄາ';

$data['lang_all'] = 'ລວມຢູ່';
$data['lang_allall'] = 'ທັງຫມົດ';

$data['lang_returntotal'] = 'ຈໍານວນເງິນຄືນ';
$data['lang_returnprice'] = 'Refund';
$data['lang_returnpriceenter'] = 'Refund(Enter)';

$data['lang_lookfoodlist'] = 'ເບິ່ງລາຍການອາຫານ';

$data['lang_cannotfound'] = 'ບໍ່ພົບຂໍ້ມູນ.';

$data['lang_type'] = 'ປະເພດ';
$data['lang_selecttype'] = 'ເລືອກຫມວດຫມູ່';

$data['lang_foodname'] = 'ຊື່ອາຫານ';

$data['lang_status'] = 'ສະຖານະພາບ';

$data['lang_searchfoodname'] = 'ຄົ້ນຫາຊື່ອາຫານ';

$data['lang_show'] = 'ສະແດງໃຫ້ເຫັນ';

$data['lang_page'] = 'ຫນ້າ';

$data['lang_saleready'] = 'Available for sale';

$data['lang_salenotready'] = 'ບໍ່ພ້ອມທີ່ຈະ';

$data['lang_room'] = 'ห้องพัก';
$data['lang_lookroom'] = 'ดูห้องพัก';
$data['lang_searchroomname'] = 'ค้นหาชื่อห้อง';
$data['lang_roomname'] = 'ชื่อห้อง';
$data['lang_roomnamenumber'] = 'ชื่อห้อง/หมายเลขห้อง';
$data['lang_bed'] = 'ที่นอน';
$data['lang_pricerent'] = 'ราคาเช่า';
$data['lang_numrent'] = 'จำนวนเช่า';

$data['lang_roomblank'] = 'ห้องว่าง';
$data['lang_roomreserved'] = 'จองแล้ว';
$data['lang_roomcheckin'] = 'เช็คอินแล้ว';
$data['lang_roomprice'] = 'ค่าห้อง';
$data['lang_searchroomnameandcus'] = 'ค้นหาห้องพัก หรือ ชื่อลูกค้า';
$data['lang_cus'] = 'ລູກຄ້າ';
$data['lang_cusname'] = 'ຊື່ລູກຄ້າ';
$data['lang_numcus'] = 'ຈຳນວນລູກຄ້າ';

$data['lang_rent'] = 'จอง';
$data['lang_checkin'] = 'เช็คอิน';
$data['lang_checkout'] = 'เช็คเอ้า';


$data['lang_foodordercheckbill'] = 'รับลูกค้า / สั่งอาหาร / เช็คบิล';
$data['lang_foodlist'] = 'รายการอาหาร';
$data['lang_foodtable'] = 'จัดโต๊ะ';
$data['lang_foodreport'] = 'รายงานการขาย';


$data['lang_index'] = 'ຫນ້າທໍາອິດ';

$data['lang_foodtableblank'] = 'โต๊ะว่าง';
$data['lang_foodorderwait'] = 'รอสั่งอาหาร';
$data['lang_foodordersuccess'] = 'สั่งอาหารแล้ว';
$data['lang_foodgetsuccess'] = 'ได้รับอาหารครบแล้ว';

$data['lang_foodtableseat'] = 'ที่นั่ง';

$data['lang_foodselect'] = 'เลือกอาหาร';
$data['lang_foodgetmoney'] = 'ຮັບເງິນ';
$data['lang_foodcustomersitdown'] = 'ลูกค้านั่งที่โต๊ะแล้ว';
$data['lang_foodlistneed'] = 'รายการอาหารที่สั่ง';

$data['lang_qty'] = 'ຈຳນວນ';
$data['lang_priceall'] = 'ລາຄາລວມ';
$data['lang_foodblank'] = 'โต๊ะว่าง';
$data['lang_delete'] = 'ລຶບ';
$data['lang_close'] = 'ປິດ';

$data['lang_selectlist'] = 'ເລືອກລາຍການ';

$data['lang_foodselectlist'] = 'เลือกรายการอาหาร';
$data['lang_foodalllist'] = 'เลือกประเภทอาหาร';

$data['lang_foodtype'] = 'ประเภทอาหาร';
$data['lang_foodlandingpage'] = 'หน้า Page รายการอาหาร';

$data['lang_foodaddnew'] = 'เพิ่มรายการอาหารใหม่';
$data['lang_foodsearchname'] = 'ค้นหาจากชื่ออาหาร';
$data['lang_foodimage'] = 'รูปอาหาร';
$data['lang_foodoutstock'] = 'อาหารหมด';
$data['lang_manage'] = 'ການຄຸ້ມຄອງ';


$data['lang_showdel'] = 'ສະແດງປຸ່ມລຶບ';

$data['lang_link'] = 'Links';

$data['lang_code'] = 'Code of Conduct';
$data['lang_exsample'] = 'ຕົວຢ່າງ';

$data['lang_tablename'] = 'ชื่อโต๊ะ/หมายเลขโต๊ะ';
$data['lang_seat'] = 'จำนวนที่นั่ง/จำนวนเก้าอี้';
$data['lang_numseat'] = 'ที่นั่ง';

$data['lang_cancel'] = 'ຍົກເລີກ';



$data['lang_rentcheckinout'] = 'จองห้องพัก / เช็คอิน / เช็คเอ้า';
$data['lang_roommanage'] = 'จัดการห้องพัก';
$data['lang_roomreport'] = 'รายงานเช่าห้องพัก';

$data['lang_salepic'] = 'ການຂາຍຮູບພາບ';
$data['lang_salelist'] = 'ຂາຍສິນຄ້າ';
$data['lang_returnproduct'] = 'Returns Product';
$data['lang_stockproduct'] = 'Warehouse Stock';
$data['lang_cusandcontact'] = 'ລູກຄ້າແລະຕິດຕໍ່ພົວພັນ';
$data['lang_salereport'] = 'ລາຍງານການຂາຍ';
$data['lang_salesetting'] = 'ກໍານົດການຂາຍ';
$data['lang_emailmarketting'] = 'Email Marketing';

$data['lang_settingdiscount'] = 'ກໍານົດລາຄາຜ່ອນຜັນ';
$data['lang_settingpricecus'] = 'ກໍານົດລາຄາສໍາລັບລູກຄ້າເທົ່ານັ້ນ';

$data['lang_priceperunit'] = 'ລາຄາຂາຍປີກ';
$data['lang_wholepriceperunit'] = 'ລາຄາຂາຍສົ່ງ';
$data['lang_discountperunit'] = 'ລາຄາຕໍ່ຫນ່ວຍ';

$data['lang_memberid'] = 'Member ID';
$data['lang_settingspacial'] = 'Set Special Sale Price';
$data['lang_person'] = 'ປະຊາຊົນ';
$data['lang_birthday'] = 'ວັນເດືອນປີເກີດ';

$data['lang_searchkeyword'] = 'ພິມຄໍາຄົ້ນຫາຂອງທ່ານ';
$data['lang_birthday'] = 'ວັນເດືອນປີເກີດ';
$data['lang_daymonth'] = 'ມື້ເດືອນ';
$data['lang_refresh'] = 'ເລີ່ມຕົ້ນໃຫມ່';

$data['lang_settingpriceproduct'] = 'Set price';

$data['lang_pricedefault'] = 'ລາຄາປະກະຕິ';
$data['lang_pricespacialforcus'] = 'ລາຄາຂາຍພິເສດສໍາລັບລູກຄ້າ';
$data['lang_editpricespacial'] = 'Fix Special Sale Price';
$data['lang_usepricedefault'] = 'ໃຊ້ລາຄາປົກກະຕິ.';
$data['lang_pricespacial'] = 'ພິເສດ';

$data['lang_subject'] = 'ຫົວຂໍ້...';
$data['lang_message'] = 'ຂໍ້ຄວາມ...';
$data['lang_messagetoallemail'] = 'ລູກຄ້າມີສະມາຊິກ. email ທຸກໆຄົນ';
$data['lang_sendemail'] = 'ສົ່ງ Email';
$data['lang_waitsendemail'] = 'ການສົ່ງລະບົບ Email ກະລຸນາລໍຖ້າ ';


$data['lang_producthavepic'] = 'ປະເພດ';
$data['lang_vat'] = 'vat';
$data['lang_cannotfoundproduct'] = '* ບໍ່ພົບຜະລິດຕະພັນ';
$data['lang_enter'] = 'enter';
$data['lang_getmoney'] = 'ໄດ້ຮັບເງິນ';
$data['lang_getmoneyenter'] = 'Enter';
$data['lang_pricesumvat'] = 'ລາຄາລວມ vat';
$data['lang_currency'] = 'ກີບ';

$data['lang_addvat'] = 'ຕື່ມຂໍ້ມູນ vat';
$data['lang_summary'] = 'ເງິນສົມທົບ';
$data['lang_moneyfromcus'] = 'ເງິນຈາກລູກຄ້າ';
$data['lang_moneychange'] = 'ເງິນທອນ';

$data['lang_ok'] = 'OK';
$data['lang_billmini'] = 'ສະຫຼຸບໂດຍຫຍໍ້';
$data['lang_billfull'] = 'ໃບຮັບເງິນຂະຫນາດເຕັມ';
$data['lang_searchcus'] = 'ຊອກຫາລູກຄ້າ';
$data['lang_addcus'] = 'ເພີ່ມລູກຄ້າ';

$data['lang_select'] = 'ເລືອກເອົາ';
$data['lang_productlist'] = 'ລາຍະການສິນຄ້າ';
$data['lang_saleproductlist'] = 'ລາຍການຂາຍ';
$data['lang_runno'] = 'Runno';

$data['lang_print'] = 'ພິມ';
$data['lang_productservice'] = 'ສິນຄ້າ / ບໍລິການ';

$data['lang_sales'] = 'ພະນັກງານ';
$data['lang_day'] = 'ມື້';
$data['lang_salelisttoday'] = 'ການຂາຍໃນມື້ນີ້';

$data['lang_pricesale'] = 'ລາຄາຊື້';
$data['lang_pricesum'] = 'ລາຄາຊື້ທັງຫມົດ';

$data['lang_productnum'] = 'ຈໍານວນຜະລິດຕະພັນ';
$data['lang_selectproduct'] = 'ເລືອກຜະລິດຕະພັນ';

$data['lang_addproductlistplz'] = 'ກະລຸນາເພີ່ມລາຍການ';
$data['lang_getmoneyplz'] = 'ກະລຸນາຮັບເງິນຈາກລູກຄ້າ';
$data['lang_getmoneymoreplz'] = 'ກະລຸນາຮັບເງິນໃຫ້ເທົ່າກັບຫຼືຫຼາຍກວ່າລາຄາຂາຍ';
$data['lang_getmoneynumberplz'] = 'ໄດ້ຮັບຄ່າຈ້າງໃນຈໍານວນ';
$data['lang_moneychangenotmore1000'] = 'ຮັບເງິນຈາກລູກຄ້າ ເງິນທອນບໍ່ຄວນເກີນ 100,000 ກີບ';


$data['lang_returnmoneyplz'] = 'ໂປດຈ່າຍຄືນລູກຄ້າ.';
$data['lang_returnmoneynumberplz'] = 'ໂປດຈ່າຍຄືນເງິນເປັນເລກ';
$data['lang_returnmoneychangenotmore1000'] = 'ຄືນເງິນໃຫ້ລູກຄ້າ ເງິນທອນບໍ່ຄວນເກີນ 100,000 ກີບ';

$data['lang_cusreturn'] = 'ປະຊາຊົນກັບຄືນ';

$data['lang_runnoref'] = 'Run no ອ້າງອິງ';
$data['lang_priceallreturn'] = 'ລາຄາລວມ';

$data['lang_priceallreturn'] = 'ລາຄາລວມ';
$data['lang_returnsuccess'] = 'ສໍາເລັດ';
$data['lang_returnlist'] = 'ລາຍະການສິນຄ້າ';

$data['lang_salereportall'] = 'ຂາຍທັງຫມົດ';

$data['lang_cusstatsalelist'] = 'ສະຖິຕິລູກຄ້າ / ການຊື້';
$data['lang_reportreturnproduct'] = 'ບົດລາຍງານຜະລິດຕະພັນ';
$data['lang_salereportsupplier'] = 'ລາຍງານການຂາຍ Supplier';
$data['lang_salereportsummary'] = 'ລາຍໄດ້ຂາຍສຸດທິ';
$data['lang_cuspoint'] = 'Member Rating';

$data['lang_salenumall'] = 'ການຊື້ທັງຫມົດ';
$data['lang_salepriceall'] = 'ຍອດເງິນທີ່ຊື້';
$data['lang_salelistall'] = 'ຊື້ສິນຄ້າ';

$data['lang_download'] = 'ດາວໂຫລດ';

$data['lang_supplier'] = 'Supplier';
$data['lang_selectsupplier'] = 'ເລືອກ Supplier';
$data['lang_selectsupplierplz'] = 'ເລືອກ Supplier';


$data['lang_cansalesummary'] = 'ຍອດຂາຍສຸດທິ';
$data['lang_revenusummary'] = 'ລາຍໄດ້ສຸດທິ';

$data['lang_score'] = 'Point';
$data['lang_editscore'] = 'ປັບປຸງຜະລິດແນນ';

$data['lang_productstock'] = 'ສິນຄ້ານຮ້ານ';
$data['lang_productintostock'] = 'ນໍາເຂົ້າສິນຄ້າ';
$data['lang_productliststock'] = 'ລາຍະການສິນຄ້າ';
$data['lang_productcategory'] = 'ຫມວດຫມູ່';
$data['lang_supplierstock'] = 'ຜູ້ຜະລິດ / ເຈົ້າຂອງ';
$data['lang_productpagepic'] = 'ຫນ້າສະແດງລາຍະການທັງຫມົດທີ່ມີຮູບພາບ';

$data['lang_remark'] = 'ຫມາຍເຫດ';
$data['lang_refnumber'] = 'ລະຫັດອ້າງອີງ';
$data['lang_fullscreen'] = 'ຫນ້າຈໍໃຫຍ່';

$data['lang_costperunit'] = 'ຄ່າໃຊ້ຈ່າຍ';
$data['lang_wherestore'] = 'Zone ການຂາຍ';
$data['lang_unit'] = 'ຈໍານວນຫນ່ວຍ';
$data['lang_allprice'] = 'ລາຄາລວມ';

$data['lang_productlistimport'] = 'ສິ່ງທີ່ນໍາເຂົ້າ';

$data['lang_importproductexcel'] = 'ນໍາເຂົ້າລາຍະການຜະລິດຕະພັນຈາກ Excel';
$data['lang_addproduct'] = '+ ຕື່ມຂໍ້ມູນສິນຄ້າ';
$data['lang_picproduct'] = 'ຮູບພາບສິນຄ້າ';

$data['lang_productlistfromexcel'] = 'ລາຍະການສິນຄ້າ Excel .CSV';
$data['lang_upload'] = 'ອັບໂຫລດ';
$data['lang_csvexsample'] = 'ຕົວຢ່າງ .CSV  UTF-8';

$data['lang_pic'] = 'ຮູບພາບ';
$data['lang_name'] = 'ຊື່';


$data['lang_cusnamelist'] = 'ລາຍຊື່ລູກຄ້າ';
$data['lang_cuscontactlist'] = 'ລາຍຊື່ຜູ້ຕິດຕໍ່';
$data['lang_statsummary'] = 'ສະຖິຕິທົ່ວໄປ';
$data['lang_statcus'] = 'ສະຖິຕິລູກຄ້າ';
$data['lang_statcontact'] = 'ຕິດຕໍ່ເຮົາ';
$data['lang_cusproductservice'] = 'ສິນຄ້າ / ບໍລິການ';
$data['lang_cusgroup'] = 'ກຸ່ມລູກຄ້າ';
$data['lang_cusrank'] = 'ລະດັບລູກຄ້າ';
$data['lang_cussex'] = 'ຂໍ້ມູນຂອງລູກຄ້າ';
$data['lang_cuscontactchanel'] = 'ຕິດຕໍ່ເລົາ';
$data['lang_cusgrade'] = 'ຊັ້ນຮຽນ / ຈຸດຕິດຕໍ່';
$data['lang_cusreasonbuy'] = 'ເປັນຫຍັງຈຶ່ງຊື້?';
$data['lang_cusreasonnotbuy'] = 'ເປັນຫຍັງຈຶ່ງບໍ່ສາມາດຊື້';

$data['lang_cusproductserviceneed'] = 'ຜະລິດຕະພັນແລະການບໍລິການ';

$data['lang_contact'] = 'ຕິດຕໍ່';
$data['lang_membercard'] = 'ສະມາຊິກບັດ';

$data['lang_selectsex'] = 'ເລືອກເພດ';
$data['lang_selectgroup'] = 'ເລືອກກຸ່ມ';
$data['lang_selectlevel'] = 'ເລືອກລະດັບ';

$data['lang_cusdetail'] = 'ລາຍະລະອຽດຂອງລູກຄ້າ';
$data['lang_cusedit'] = 'ແກ້ໄຂລາຍຊື່ລູກຄ້າ';

$data['lang_detail'] = 'ລາຍະລະອຽດ';

$data['lang_addnewcontact'] = 'ຕື່ມການຕິດຕໍ່ໃຫມ່';
$data['lang_detailcontact'] = 'ລາຍະລະອຽດການຕິດຕໍ່';

$data['lang_selectchanel'] = 'ເລືອກຊ່ອງທາງຕິດຕໍ່';
$data['lang_selectgrade'] = 'ເລືອກການຈັດອັນດັບ / ຕິດຕໍ່ຕິດຕໍ່';
$data['lang_selectproductservice'] = 'ເລືອກຜະລິດຕະພັນທີ່ຫນ້າສົນໃຈ';
$data['lang_selectreasonbuy'] = 'ເລືອກເຫດຜົນທີ່ຈະຊື້.';
$data['lang_selectreasonnotbuy'] = 'ເອົາເຫດຜົນທີ່ບໍ່ໃຫ້ຊື້.';

$data['lang_editcontact'] = 'ແກ້ໄຂຕິດຕໍ່';

$data['lang_list'] = 'ລາຍະການ';
$data['lang_stat'] = 'ສະຖິຕິ';

$data['lang_bar'] = 'Bar graph';
$data['lang_donut'] = 'Donut chart';
$data['lang_downloadcus'] = 'ດາວໂຫລດລາຍຊື່ລູກຄ້າ';
$data['lang_downloadproductservice'] = 'ດາວໂຫລດຜະລິດຕະພັນ / ບໍລິການ';
$data['lang_downloadcontact'] = 'ດາວໂຫລດລາຍລະອຽດການຕິດຕໍ່';
$data['lang_downloadlistcontact'] = 'ດາວໂຫລດລາຍຊື່ຜູ້ຕິດຕໍ່';

$data['lang_date'] = 'ມື້';
$data['lang_month'] = 'ເດືອນ';
$data['lang_months'] = 'ເດືອນ';
$data['lang_year'] = 'ປີ';

$data['lang_addnewproductservice'] = 'ເພີ່ມຜະລິດຕະພັນ / ບໍລິການໃຫມ່';
$data['lang_editnewproductservice'] = 'ສິນຄ້າ / ບໍລິການໃຫມ່';

$data['lang_addnewgroup'] = 'ເພີ່ມລູກຄ້າໃຫມ່';
$data['lang_editgroup'] = 'ແກ້ໄຂບັນດາລູກຄ້າ';

$data['lang_addnewlevel'] = 'ເພີ່ມລູກຄ້າໃຫມ່';
$data['lang_editlevel'] = 'ແກ້ໄຂລະດັບລູກຄ້າ';

$data['lang_addnewsex'] = 'ຕື່ມການລວມຂໍ້ມູນໃຫມ່';
$data['lang_editsex'] = 'ປັບປຸງການລວມຂໍ້ມູນຂອງລູກຄ້າ';

$data['lang_addnewchanel'] = 'ຕື່ມການຕິດຕໍ່ໃຫມ່';
$data['lang_editchanel'] = 'ແກ້ໄຂຕິດຕໍ່ລູກຄ້າ';

$data['lang_addnewgrade'] = 'ເພີ່ມລູກຄ້າໃຫມ່';
$data['lang_editgrade'] = 'ແກ້ໄຂລູກຄ້າ';

$data['lang_addnewreasonbuy'] = 'ເພີ່ມເຫດຜົນທີ່ຈະຊື້ໃຫມ່';
$data['lang_editreasonbuy'] = 'ເຫດຜົນທີ່ຈະຊື້';

$data['lang_addnewreasonnotbuy'] = 'ເພີ່ມເຫດຜົນທີ່ບໍ່ໃຫ້ຊື້ໃຫມ່';
$data['lang_editreasonnotbuy'] = 'ເຫດຜົນສໍາລັບການບໍ່ຢາກຊື້';




//login

$data['lang_adminlogin'] = 'ການຄຸ້ມຄອງ(Admin)';
$data['lang_managelogin'] = 'ຜູ້ຈັດການ';
$data['lang_stafflogin'] = 'ພະນັກງານ';



$data['lang_pagelogin'] = 'ເຂົ້າສູ່ລະບົບ';
$data['lang_loginusername'] = 'User Name';
$data['lang_loginpassword'] = 'ລະຫັດຜ່ານ';
$data['lang_loginemail'] = 'ທີ່ຢູ່ອີເມວ';


$data['lang_cannotlogin'] = 'ບໍ່ສາມາດເຂົ້າສູ່ລະບົບໄດ້! ຊື່ຜູ້ໃຊ້ຫຼືລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ.';
$data['lang_loginplz'] = 'ກະລຸນາໃສ່ຊື່ຜູ້ໃຊ້ແລະລະຫັດຜ່ານ!';
$data['lang_loginemailplz'] = 'ກະລຸນາໃສ່ອີເມວແລະລະຫັດຜ່ານຂອງທ່ານ!';


$data['lang_retail'] = 'ຂາຍຍ່ອຍ';
$data['lang_wholesale'] = 'ຂາຍສົ່ງ';

$data['lang_cash'] = 'ເງິນສົດ';
$data['lang_transfer'] = 'ການໂອນເງິນ';
$data['lang_creditcard'] = 'Credit Cards';
$data['lang_owe'] = 'ເກີນກວ່າ';

$data['lang_bookitem'] = 'ຈອງ';

$data['lang_mo'] = 'ເງິນ';
$data['lang_discount_last'] = 'ຈໍານວນເງິນທີ່ຫຼຸດລົງ';
$data['lang_dis'] = 'ຫຼຸດລົງ';

$data['lang_sumall'] = 'ສຸດທິ';
$data['lang_payby'] = 'ຈ່າຍໂດຍ';

$data['lang_outofstock'] = 'Out of stock';

$data['lang_balance'] = 'ຊ້າຍ';
$data['lang_piece'] = 'ຈໍານວນ';

$data['lang_saleoutdoor'] = 'ຈໍານວນ';

$data['lang_gotostockbig'] = 'ໄປຮ້ານຄ້າຫຼັກຊັບ';
$data['lang_gotostockstore'] = 'ໄປຮ້ານຄ້າຫຼັກຊັບ';
$data['lang_gotostocklol'] = 'ໄປຫາສິນຄ້າຄົງຄັງ';

$data['lang_savegotosuccess'] = 'ทำรายการนำออกสินค้าสำเร็จ';

$data['lang_printsend'] = 'ພິມອອກ';
$data['lang_listproductout'] = 'รายการสินค้านำออก';

$data['lang_slipsendproduct'] = 'ການຂົນສົ່ງ';

$data['lang_pay'] = 'ການຊໍາລະເງິນ';
$data['lang_by'] = 'ບຸກຄົນການຄ້າ';

$data['lang_oksumall'] = 'ສຸດທິສຸດທິ';
$data['lang_slip'] = 'ໃບຮັບເງິນ';

$data['lang_saletoday'] = 'ຂາຍວັນນີ້';
$data['lang_income'] = 'ລາຍໄດ້';
$data['lang_productsaletoday'] = 'ຂາຍດີໃນມື້ນີ້.';
$data['lang_cussaletoday'] = 'ລູກຄ້າຊື້ດີໃນມື້ນີ້.';
$data['lang_productwillout'] = 'ຜະລິດຕະພັນໃກ້ກັບຫຼັກຊັບ';

$data['lang_stockstore'] = 'ຮ້ານຄ້າຫຼັກຊັບ';
$data['lang_stockbig'] = 'Great Warehouse';

$data['lang_pointsalelol'] = 'ຂາຍໃນຄັງສິນຄ້າ';
$data['lang_stocklol'] = 'ຈຸດຂອງການຂາຍ';

$data['lang_billreserv'] = 'ບໍ່ໄດ້ຈ່າຍ/ຈອງ';

$data['lang_searchname'] = 'ຄົ້ນຫາ';


$data['lang_pricebeforvat'] = 'ລາຄາກ່ອນ vat';




// 3/7/2018
$data['lang_postitle'] = 'ໂປຼແກຼມມິນິມາກ';

$data['lang_clickthissalesummary'] = 'ຄລິກເພື່ອເບິ່ງ ສະຫຼຸບຍອດຂາຍ';
$data['lang_salewhatshif'] = 'ຍອດຂາຍກະທີ';
$data['lang_notopenshif'] = 'ຍັງບໍ່ໄດ້ເປີດກະ';
$data['lang_salegoodshif'] = 'ຂາຍດີກະທີ';
$data['lang_clickthissaleitem'] = 'ຄລິກເພື່ອເບິ່ງສິນຄ້າຂາຍດີ';
$data['lang_clickthisstockall'] = 'ຄລິກເພື່ອເບິ່ງ Stock ທັງໝົດ';
$data['lang_clickthisalllist'] = 'ຄລິກເພື່ອເບິ່ງ ລາຍການຂາຍທັງໝົດ';
$data['lang_productenddate'] = 'ສິນຄ້າໃກ້ຈະໝົດອາຍຸ';

$data['lang_salepic'] = 'ໜ້າຈໍຂາຍສິນຄ້າ';
$data['lang_productstock'] = 'ສິນຄ້າ/ສະຕ໋ອກ';
$data['lang_customer'] = 'ລູກຄ້າ';
$data['lang_salereport'] = 'ລາຍງານການຂາຍ';
$data['lang_userstore'] = 'ພະນັກງານ/ຮ້ານ';
$data['lang_printer'] = 'ເຄື່ອງພິມ';
$data['lang_moniterforcus'] = 'ໜ້າຈໍສຳລັບລູກຄ້າ';

$data['lang_productreturn'] = 'ຄືນສິນຄ້າ';
$data['lang_stock'] = 'ສະຕ໋ອກ';
$data['lang_closeshif'] = 'ປິດກະ';
$data['lang_num'] = 'ຈຳນວນ';
$data['lang_product'] = 'ສິນຄ້າ';
$data['lang_productunit'] = 'ໜ່ວຍນັບ';
$data['lang_sum'] = 'รวม';
$data['lang_discountlast'] = 'ສ່ວນຫຼຸດທ້າຍບິນ';

$data['lang_retailx'] = 'ຂາຍປີກ';
$data['lang_wsale'] = 'ຂາຍສົ່ງ';
$data['lang_getmoneyx'] = 'ຮັບເງິນ';
$data['lang_add'] = 'ເພີ່ມ';
$data['lang_exdel'] = 'ນຳອອກ';
$data['lang_group'] = 'ກຸ່ມ';
$data['lang_billall'] = 'ບິນຮັບເງິນ/ໃບສົ່ງສິນຄ້າ';
$data['lang_payer'] = 'ຜູ້ຈ່າຍເງິນ';
$data['lang_namezen'] = 'ລົງຊື່';
$data['lang_geter'] = 'ຜູ້ຮັບເງິນ';

$data['lang_otlistof'] = 'ລາຍການສິນຄ້າເສີມ';
$data['lang_selectbiga4'] = 'ເລືອກໃບຕິດໜ້າກ່ອງໃຫຍ່ ເຈ້ຍ A4';
$data['lang_selectmini'] = 'ເລືອກໃບຕິດໜ້າກ່ອງນ້ອຍ';
$data['lang_sender'] = 'ຜູ້ສົ່ງ';
$data['lang_receiver'] = 'ຜູ້ຮັບ';
$data['lang_printbillshif'] = 'ພິມບິນປິດຍອດກະ';
$data['lang_newopenshif'] = 'ເປີດກະການຂາຍໃໝ່';
$data['lang_startmoney'] = 'ເງິນໃນລີ້ນຊັກເລີ່ມຕົ້ນ';
$data['lang_startsale'] = 'ເລີ່ມຂາຍ';
$data['lang_goindex'] = 'ໄປໜ້າທຳອິດ';
$data['lang_closeshifsendmoney'] = 'ປິດກະສົ່ງເງິນ';
$data['lang_moneyindrawer'] = 'ເງິນໃນລີ້ນຊັກ';
$data['lang_confirmcloseshif'] = 'ຢືນຢັນການປິດກະ';
$data['lang_waitopenshif'] = 'ກະລຸນາຖ້າເປີດກະ';

$data['lang_billcloseshif'] = 'ບິນປິດຍອດ ກະທີ ';
$data['lang_start'] = 'ເລີ່ມ';
$data['lang_to'] = 'ເຖິງ';
$data['lang_endmoney'] = 'ເງິນໃນລີ້ນຊັກສຸດທ້າຍ';
$data['lang_endstart'] = 'ສ່ວນຕ່າງເງິນໃນລີ້ນຊັກ';
$data['lang_num'] = 'ຈຳນວນ';
$data['lang_getmoneyfromcus'] = 'ຮັບເງິນຈາກລູກຄ້າ';
$data['lang_deleteall'] = 'ລຶບທັງໝົດ';
$data['lang_lastsale'] = 'ລາຍການຂາຍລ້າສຸດ';
$data['lang_printbox'] = 'ພິມໃບຕິດໜ້າກ່ອງ';
$data['lang_numberplz'] = 'ກະລຸນາໃສ່ຕົວເລກ';
$data['lang_printing'] = 'ກຳລັງພິມ...';
$data['lang_saving'] = 'ກຳລັງບັນທຶກລາຍການ...';
$data['lang_savingsuccess'] = 'ບັນທຶກລາຍການຂາຍສຳເລັດ...';

$data['lang_productrelat'] = 'ສິນຄ້າສຳພັນ(ແກ໋ດ/ຕຸກ...)';
$data['lang_mini'] = 'ນ້ອຍ';
$data['lang_big'] = 'ໃຫຍ່';
$data['lang_design'] = 'ອອກແບບ';
$data['lang_alertwhen'] = 'ເຕືອນເມື່ອເຫຼືອ';
$data['lang_relaydetail'] = 'ເຊັ່ນ 1 ລັງ ມີ 12 ແພ໋ກ , 1 ລັງ ມີ  144 ແກ້ວ';
$data['lang_alertwhennum'] = 'ແຈ້ງເຕືອນເມື່ອຈຳນວນເຫຼືອນ້ອຍກວ່າ';

$data['lang_productoutstock'] = 'ສິນຄ້າໝົດສະຕ໋ອກ';
$data['lang_producthavestock'] = 'ສິນຄ້າຄົງເຫຼືອ';
$data['lang_productgooutstock'] = 'ນຳອອກສິນຄ້າ';
$data['lang_productzone'] = 'Zone ສິນຄ້າ';

$data['lang_showgoout'] = 'ສະແດງປຸ່ມ ນຳອອກ';
$data['lang_showgosale'] = 'ສະແດງປຸ່ມ ນຳໄປຂາຍ';
$data['lang_dateend'] = 'ວັນໝົດອາຍຸ';
$data['lang_dategetin'] = 'ວັນທີນຳເຂົ້າ';
$data['lang_goout'] = 'ນຳອອກ';
$data['lang_gosale'] = 'ນຳໄປຂາຍ';

$data['lang_billgoin'] = 'ໃບນຳເຂົ້າສິນຄ້າ';

$data['lang_billgoout'] = 'ໃບນຳອອກສິນຄ້າ';
$data['lang_stocker'] = 'ຜູ້ເບິ່ງແຍງຄັງສິນຄ້າ';
$data['lang_productreceiver'] = 'ຜູ້ຮັບສິນຄ້າ';

$data['lang_productunitdetail'] = 'ໜ່ວຍນັບ ເຊັ່ນ ແກ້ວ';

$data['lang_salereportshif'] = 'ຍອດຂາຍຕາມກະ';
$data['lang_salereportcategory'] = 'ຍອດຂາຍໝວດໝູ່';
$data['lang_summaryall'] = 'ສະຫຼຸບຍອດຂາຍທັງໝົດ';
$data['lang_profitday'] = 'ກຳໄລ ລາຍວັນ';
$data['lang_profitmon'] = 'ກຳໄລ ລາຍເດຶອນ';

$data['lang_shift'] = 'ກະທີ';
$data['lang_by'] = 'ໂດຍ';
$data['lang_timeopenshift'] = 'ເວລາທີ່ເປີດກະ';
$data['lang_timecloseshift'] = 'ເວລາທີ່ປິດກະ';
$data['lang_salenum'] = 'ຈຳນວນສິນຄ້າທີ່ຂາຍໄດ້';
$data['lang_allsaleprice'] = 'ຍອດຂາຍ';

$data['lang_getmonetallsaleproduct'] = 'ລາຍຮັບລວມ ຈາກການຂາຍສິນຄ້າ';

$data['lang_waypayment'] = 'ຊ່ອງທາງການຊຳລະເງິນ';

$data['lang_month'] = 'ເດືອນ';
$data['lang_year'] = 'ປີ';
$data['lang_profit'] = 'ກຳໄລ';

$data['lang_jan'] = 'ມັງກອນ';
$data['lang_feb'] = 'ກຸມພາ';
$data['lang_mar'] = 'ມີນາ';
$data['lang_april'] = 'ເມສາ';
$data['lang_may'] = 'ພຶດສະພາ';
$data['lang_jun'] = 'ມິຖຸນາ';
$data['lang_july'] = 'ກໍລະກົດ';
$data['lang_orgus'] = 'ສິງຫາ';
$data['lang_sep'] = 'ກັນຍາ';
$data['lang_org'] = 'ຕຸລາ';
$data['lang_novem'] = 'ພະຈິກ';
$data['lang_desam'] = 'ທັນວາ';

$data['lang_jan2'] = 'ມັງກອນ';
$data['lang_feb2'] = 'ກຸມພາ';
$data['lang_mar2'] = 'ມີນາ';
$data['lang_april2'] = 'ເມສາ';
$data['lang_may2'] = 'ພຶດສະພາ';
$data['lang_jun2'] = 'ມິຖຸນາ';
$data['lang_july2'] = 'ກໍລະກົດ';
$data['lang_orgus2'] = 'ສິງຫາ';
$data['lang_sep2'] = 'ກັນຍາ';
$data['lang_org2'] = 'ຕຸລາ';
$data['lang_novem2'] = 'ພະຈິກ';
$data['lang_desam2'] = 'ທັນວາ';

$data['lang_settingpricecusgroup'] = 'ตั้งค่าราคาสินค้าเฉพาะกลุ่มลูกค้า ຕັ້ງຄ່າລາຄາສິນຄ້າສະເພາະກຸ່ມລູກຄ້າ';
$data['lang_pricecusgroup'] = 'ລາຄາຂາຍພິເສດສຳລັບກຸ່ມ';

$data['lang_staff'] = 'ພະນັກງານ';
$data['lang_staffsale'] = 'ພະນັກງານຂາຍສິນຄ້າ';
$data['lang_staffstock'] = 'ພະນັກງານສະຕ໋ອກຮ້ານ';
$data['lang_cusseestock'] = 'ລູກຄ້າເບິ່ງສະຕ໋ອກຮ້ານ';
$data['lang_staffadmin'] = 'Admin ຮ້ານ';


?>
